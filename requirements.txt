h5py
mpi4py
matplotlib >=3.1.0
netCDF4
numba
numpy >=1.16.0
pandas >= 1.0.0
scipy
tables
emcee ==3.0.2
schwimmbad ==0.3.1
