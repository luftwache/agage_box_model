# -*- encoding: utf-8 -*-

r"""
setup. py

AGAGE Box Model for isotopologue modelling

"""
# Standard Library imports
import io
import sys

# Third party imports
from setuptools import find_packages, setup


# =============================================================================
# Main Constants
# =============================================================================
NAME = "chung_box_model"
LIBNAME = "chung_box_model"
VERSION = "1.0.0"

# =============================================================================
# Check for Python 3
# =============================================================================
PY3 = sys.version_info[0] == 3
v = sys.version_info
if v[0] >= 3 and v[:2] < (3, 6):
    error = "ERROR: Requires Python version 3.6 or above."
    print(error, file=sys.stderr)
    sys.exit(1)

# =============================================================================
# Constants
# =============================================================================
with io.open('README.md', encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

#==============================================================================
# Setup arguments
#==============================================================================
setup_args = dict(
    name=NAME,
    version=VERSION,
    description='Box Model for Isotopologues',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author="Edward Chung",
    author_email="eunchong.chung@outlook.com",
    license='MIT',
    keywords='',
    platforms=["Windows", "Linux", "Mac OS-X"],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        ],
    )

install_requires = [
    # in conda base
    'h5py',
    'mpi4py',
    'matplotlib>=3.1.0',
    'netCDF4',
    'numba',
    'numpy>=1.16.0',
    'pandas>=1.0.0',
    'scipy',
    'tables',
    # not in conda base
    'emcee>=3.0.2',
    'schwimmbad>=0.3.1'
    ]

setup_args['install_requires'] = install_requires
setup_args.pop('scripts', None)

# =============================================================================
# Main Setup
# =============================================================================
setup(**setup_args)

