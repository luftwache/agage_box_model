# AGAGE-12box model

## Inversion

### Setup

1. Download the package.
2. Choose a python virtual environment (optional.)
3. Install dependencies.
```
conda install --file requirements.txt
```
or
```
pip install -r requirements.txt
```
4. Run setup.py in a chosen python virtual environment.

```
python setup.py develop
```
or
```
python setup.py install
```

### Usage

Run `inversion.py` in the `scripts/` to run the inversion model.

```
python3 inversion.py [-s path_to_settings_file] [-o output directory] [-t test_flag] [-v path] [-f forward_only_flag] [-n mpi_array]
```

| Parameter | Default   | Description   |
| :-------: | :-------: | :------------ |
| -s        |           | Path to the settings file. |
| -t        | 0         | Test flag. <br> 0: off, default <br> 1: test pre-processing and forward model <br> 2: same as 0 but with post-processing <br> 3: test inversion process (using multiprocessing not MPI) |
| -v        |           | Path to the output folder. Runs the model with the median posterior of the given output as input. |
| -f        | 0         | If 1, run the forward model once, disabling inversion. Same as __-t 2__ but will create output directory with trailing "f". |
| -n        |           | Number in the array in array jobs. |

The settings file in .json format should contain the following.

| Parameter     | Description   |
| :-----------: | :------------ |
| `years`       | list of int, [start year, end year]. Temporal range of model. |
| `chain`       | list of int, [ensemble size, chain length, chain repeats] |
| `method`      | int, Inversion method. Only 0 (emcee) is implemented. |
| `idir`        | path, Directory containing base input files. |
| `sce_run`     | list of int, Scenario defined in the `routines_CH4`. |
| `sce_def`     | null of list, [path, [variable names]]. Variables from previous result to use as defaults (may be modified). |
| `sce_pre`     | null of list, [path, [variable names]]. Variables from previous result to use as presets (not modified). |
| `obs_def`     | null of list, [path, [variable names]]. Variables from previous result to use as pseudo-observations. |
| `y_var`       | list, [variable names]. Observation vector. |
| `theta_var`   | list, [variable names]. State vector. |
| `model_var`   | list, [variable names]. Model constants. |
| `tile`        | dict, {variable name: [tile parameter, resulting temporal resolution]}. Variables to tile data. |
| `pslice`      | dict, {variable name: slice parameter} ]. Variables where only part of the data are included in the inversion. |
| `D_obs`       | list of int, Ambient clumped signature. |
| `D_q`         | list of int, Source clumped signature. |
| `iso_sel`     | list of int, Isotopologues to include in the sum. |

Presets used by the author is included in the `settings/`.

#### Scenario numbering
Model input is determined in three stages.
1. Read default inputi `mcmc_CH4,get_var()`.
2. Read (user-selected) variables from a previous run (`sce_def`).
3. Modify (pre-selected) variables determined by `sce_run`
4. Read (user-selected) variables from a previous run (`sce_pre`).
Each step overwrites results from previous steps.

Find the corresponding presets in the settings/ folder.
The individual settings files lists variables whose values will be based on a inversion posterior.

| sce\_run  | Description   |
| :-------: | :------------ |
| 0, 0, 0   | base, use stage 2 as is. |
| 0, 1, 0   | same as base, dummy flag for dq\_total bein a parameter not variable |
| 0, 2, 0   | same as base, dummy flag for OH\a bein a parameter not variable |
| 0, 3 ,0   | KIE of non\_OH set to half of KIE\_Cl (default is 1). |
| 0, 11, 0  | OH increasing within posterior of `sce_def`. |
| 0, 12, 0  | OH decreasing within posterior of `sce_def`. |
| 0, 13, 0  | Q increasing within posterior of `sce_def`. |
| 0, 14, 0  | Q decreasing within posterior of `sce_def`. |
| 0, 15, 0  | Qff increasing within posterior of `sce_def`. |
| 0, 16, 0  | Qff decreasing within posterior of `sce_def`. |
| x, x, 1   | Remove tropospheric non\_OH sink. |

### Creating plots
Example to create plots are given in the `scripts/final_plots.py`.

