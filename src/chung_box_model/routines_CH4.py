# -*- coding: utf-8 -*-
r"""
routines_CH4.py

MCMC routines for CH4.

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20200822    EC  Packaged.
"""
# Standard Library imports
import json
import numpy as np
import os
import pandas as pd
import re
import sys

# Third part6y imports
from collections import OrderedDict

# Local imports
from chung_box_model.chem_CH4 import CH4_table
from chung_box_model.calc import *
from chung_box_model.iso_model import Model
from chung_box_model.mcmc_CH4 import *
from chung_box_model.mcmc_utils import *


class Settings():
    def __init__(self):
        r"""Settings class used for the inversion model

        """
        self.test = 0
        self.years = [0, 0]
        self.chain = [0, 0, 0]
        self.method = 0
        self.bdir = ""
        self.idir = ""
        self.odir = ""
        self.sce_run = [0, 0, 0]
        self.sce_def = False
        self.sce_pre = False
        self.obs_def = False
        self.y_var = []
        self.theta_var = []
        self.model_var = []
        self.tile = {}
        self.pslice = {}
        self.D_obs = []
        self.D_q = {}
        self.iso_sel = []
        self.steady = False

    def read(self, ifile):
        r"""Read settings from .json file

        Parameter
        ---------
        ifile : path-like
            Input file.

        """
        with open(ifile, 'r') as f:
            idata = json.load(f)
        for k, v in idata.items():
            if k == 'pslice':
                setattr(self, k, {k1: eval(v1) for k1, v1 in v.items()})
            else:
                setattr(self, k, v)

    def write(self, ofile):
        r"""Write settings to .json file

        Parameter
        ---------
        ofile : path-like
            Output file.

        """
        # not using self.__dict__ to force order
        keys = [
                'test', 'years', 'chain', 'method',
                'bdir', 'idir', 'odir',
                'sce_run', 'sce_def', 'sce_pre', 'obs_def',
                'y_var', 'theta_var', 'model_var',
                'tile', 'pslice', 'D_obs', 'D_q',
                'iso_sel', 'steady'
                ]
        odata = OrderedDict()
        for k in keys:
            if k == 'pslice':
                odata[k] = {k1: str(v1) for k1, v1 in getattr(self, k).items()}
            else:
                odata[k] = getattr(self, k)
        with open(ofile, 'w+') as f:
            json.dump(odata, f, indent=4)


def io_r_settings(ifile):
    r"""Read settings from file

    Parameter
    --------
    ifile : path-like
        Input file.

    Returns
    -------
    oset : Settings
        Settings.

    """
    oset = Settings()
    oset.read(ifile)
    return oset


def prepare_scenario(iset):
    r"""

    Parameters
    ----------
    iset : Settings
        Input settings.

    Returns
    -------
    model : Model
        Model.
    dates : dict
        {temporal resolution: pd.date_range}
    p1, p2, p3 : dict
        {variable: value}
        Distribution parameters.
                        p1      p2          p3
            normal      mean    std dev     'n'
            uniform     lower   upper bound 'u'
            m. normal   mean    covaricance 'n2'
    ptres : dict
        {variable: temporal resolution}
        Temporal resolution of variables.
    p1_def, p2_def, p3_def : dict
        Unmodified p1, p2 and p3.
    D_q : dict
        {variable: [D_13CH3D, D_12CH2D2]}

    """
    # Initialise model object
    model = Model(
                iso=['C12H4', 'C13H4', 'C12H3D', 'C13H3D', 'C12H2D2'],
                iso_table=CH4_table
                )

    # Initialise Arrays
    pslice = {}
    D_q = {}

    # Read default input data
    p1, p2, p3, ptres, pdate, pmeta = get_vars(iset.years, iset.idir)

    # Tiling
    for k, v in iset.tile.items():
        if k == 'dq':
            sectors_d = [
                    s
                    for s in iset.theta_var + iset.model_var
                    if s.startswith('dq_')
                    ]
            for s in sectors_d:
                if not ptres[s] == 'ic':
                    continue
                p1[s] = np.tile(p1[s], v[0])
                p2[s] = np.tile(p2[s], v[0])
                ptres[s] = v[1]
        else:
            p1[k] = np.tile(p1[k], v[0])
            p2[k] = np.tile(p2[k], v[0])
        if v[1]:
            ptres[k] = v[1]

    # pslice for q and dq
    if 'q' in iset.pslice:
        sectors_q = [
                s
                for s in iset.theta_var + iset.model_var
                if s.startswith('q_')
                ]
        for s in sectors_q:
            iset.pslice[s] = iset.pslice['q']
        del iset.pslice['q']
    if 'dq' in iset.pslice:
        sectors_d = [
                s
                for s in iset.theta_var + iset.model_var
                if s.startswith('dq_')
                ]
        for s in sectors_d:
            iset.pslice[s] = iset.pslice['dq']
        del iset.pslice['dq']

    # Dates
    dates = {k: pdate[k] for k in ['1M', '1Y', '5Y', '10Y'] if k in pdate}

    # Stages and settings
    stages = []
    ivar_run = iset.theta_var + iset.model_var
    if iset.obs_def is not None:
        stages.append('obs')
        ifile = os.path.join(iset.obs_def[0], 'settings.json')
        iset_obs = io_r_settings(ifile)
    if iset.sce_def is not None:
        stages.append('def')
        ifile = os.path.join(iset.sce_def[0], 'settings.json')
        iset_def = io_r_settings(ifile)
        ivar_def = iset_def.theta_var + iset_def.model_var
    if iset.sce_pre is not None:
        stages.append('pre')
        ifile = os.path.join(iset.sce_pre[0], 'settings.json')
        iset_pre = io_r_settings(ifile)
        ivar_pre = iset_pre.theta_var + iset_pre.model_var
    if all([k in stages for k in ['def', 'pre']]):
        if [k for k in iset.sce_def[1] if k in iset.sce_pre[1]]:
            raise Exception('Default and preset variables clash.')

    # =========================================================================
    # Observations
    # =========================================================================
    if 'obs' in stages:
        ifile = os.path.join(iset.obs_def[0], 'out_c.npy')
        iy = calc_y(
                iset_obs.y_var,
                model.iso,
                np.load(ifile, 'r'),
                iset_obs.iso_sel,
                )
        arrshp = {
                k:
                np.load(os.path.join(iset.obs_def[0], 'p1.npz'), 'r')[k].shape
                for k in iset_obs.y_var
                }
        for k in iset_obs.y_var:
            p1[k] = np.median(iy[k].reshape(-1, *arrshp[k]), axis=0)

    # Remove ambient clumped signatures
    y_clumped = [i for i in ['D_C13H3D', 'D_C12H2D2'] if i in iset.y_var]
    if iset.sce_run[0] in [0, 1] and y_clumped:
        for i in y_clumped:
            p1[i] *= 0.0
            p1[i] += np.nan
            p2[i] *= np.nan

    # =========================================================================
    # State - First stage - Read defaults
    # =========================================================================
    if iset.sce_run[0] in [0, 1] and iset.sce_run[1] in [3, 4]:
        p1['KIE_tau'] = np.tile(
                np.array(
                    CH4_table['KIE_Cl'][model.iso[1:]]*.5 + .5
                    )[np.newaxis, :, np.newaxis],
                (1, 1, 12)
                )
        p1['KIE_tau'][..., 4:8] = np.array(
                CH4_table['KIE_Cl'][model.iso[1:]]
                )[np.newaxis, :, np.newaxis]
        p2['KIE_tau'] = p1['KIE_tau'] * .2

    if iset.sce_run[0] in [0, 1] and iset.sce_run[1] in [5, 6]:
        p1['OH_a'] = np.array(
                pd.read_hdf(
                    os.path.join(idir, 'prior_rigby2017_OH.hdf'), 'OH_a'
                    )
                )
    if 'def' in stages:
        read_p(iset.sce_def, p1, p2, p3, iset_def)
        sector_d = [
            s for s in iset.sce_def[1] if s.startswith('dq_')
            ]
        if sector_d:
            ifile1 = os.path.join(iset.sce_def[0], 'p1_def.npz')
            ifile2 = os.path.join(iset.sce_def[0], 'p2_def.npz')
            ifile3 = os.path.join(iset.sce_def[0], 'p3_def.npz')
            d_q = calc_bounds(
                    {k: np.load(ifile1, 'r')[k] for k in sector_d},
                    {k: np.load(ifile2, 'r')[k] for k in sector_d},
                    {k: np.load(ifile3, 'r')[k] for k in sector_d}
                    )[0]
            R_q = {
                    k: (d_q[k]*1.e-3 + 1.) * model.R_std[1:]
                    for k in sector_d
                    }
            i1 = np.s_[..., 0, :]
            i2 = np.s_[..., 1, :]
            i3 = np.s_[..., 2, :]
            i4 = np.s_[..., 3, :]
            D_q = {
                    k:
                    np.moveaxis(
                        np.array([
                            (v[i3]/v[i1]/v[i2] - 1.) * 1.e3,
                            (v[i4]/v[i2]**2 * 8./3. - 1.) * 1.e3
                            ]),
                        0, -2
                        )
                    for k, v in R_q.items()
                    }
            for k, v in D_q.items():
                p1[k] = Delta2delta_CH4(
                        include_main_iso(p1[k], 0.), v, model.R_std
                        )[..., 1:, :]

    # =========================================================================
    # State - Second stage - Running scenario
    # =========================================================================
    # Calculate default D_q for future use
    sector_d = [
            s for s in iset.theta_var+iset.model_var if s.startswith('dq_')
            ]
    d_q = calc_bounds(
            {k: v for k, v in p1.items() if k in sector_d},
            {k: v for k, v in p2.items() if k in sector_d},
            {k: v for k, v in p3.items() if k in sector_d}
            )[0]
    R_q = {
            k: (d_q[k]*1.e-3 + 1.) * model.R_std[1:]
            for k in sector_d
            }
    i1 = np.s_[..., 0, :]
    i2 = np.s_[..., 1, :]
    i3 = np.s_[..., 2, :]
    i4 = np.s_[..., 3, :]
    D_q = {
            k:
            np.moveaxis(
                np.array([
                    (v[i3]/v[i1]/v[i2] - 1.) * 1.e3,
                    (v[i4]/v[i2]**2 * 8./3. - 1.) * 1.e3
                    ]),
                0, -2
                )
            for k, v in R_q.items()
            }

    scenario = iset.sce_run[1]
    if scenario in np.arange(11, 21):
        # Different OH pathways
        if scenario in [11, 12]:
            k_i = 'OH_a'
            k_o = 'OH_a'
        # Add emission
        if scenario in [13, 14]:  # total
            k_i = 'q_total'
            k_o = 'q_total'
        elif scenario in [15, 16]:  # fossil fuel
            k_i = 'q_total'
            k_o = 'q_ftot'
        elif scenario in [17, 18]:  # natural microbial
            k_i = 'q_total'
            k_o = 'q_mnat'
        if 'def' in stages and k_i in iset_def.theta_var:
            ifile0 = os.path.join(iset.sce_def[0], 'out_theta.npz')
            ifile1 = os.path.join(iset.sce_def[0], 'p1.npz')
            start = 50
            if scenario % 2 == 1:
                end = 84
            else:
                end = 16
            scale = 1.
            if k_i in iset_def.pslice:
                loc = iset_def.pslice[k_i]
            else:
                loc = np.s_[:]
            ndim =  (
                    np.load(ifile0, 'r')[k_i].ndim
                    - np.load(ifile1, 'r')[k_i][loc].ndim
                    )
            ps, pd = calc_gradual_weights(
                    *np.percentile(
                        np.load(ifile0, 'r')[k_i],
                        [start, end],
                        axis=tuple(range(ndim))
                        ),
                    scale
                    )
        else:
            loc = np.s_[:]
            if 'def' in stages and k_i in iset_def.model_var:
                ifile1 = os.path.join(iset.sce_def[0], 'p1.npz')
                ifile2 = os.path.join(iset.sce_def[0], 'p2.npz')
                ifile3 = os.path.join(iset.sce_def[0], 'p3.npz')
                pl, pm, pu = calc_bounds(
                        {k_i: np.load(ifile1, 'r')[k_i]},
                        {k_i: np.load(ifile2, 'r')[k_i]},
                        {k_i: np.load(ifile3, 'r')[k_i]}
                        )
            else:
                pl, pm, pu = calc_bounds(
                        {k_i: p1[k_i]},
                        {k_i: p2[k_i]},
                        {k_i: p3[k_i]}
                        )
            start = pm
            if scenario % 2 == 1:
                end = pu
            else:
                end = pl
            ps, pd = calc_gradual_weights(start, end, scale)
        if k_i == k_o:
            p1[k_o][loc] = ps + pd
        else:
            p1[k_i][loc] = ps
            p1[k_o][loc] = pd

    # =========================================================================
    # State - Third stage - Load preset values
    # =========================================================================
    if 'pre' in stages:
        read_p(iset.sce_pre, p1, p2, p3, iset_pre)

    # =========================================================================
    # State - Final stage
    # =========================================================================
    # Pseudo intial signature
    if iset.D_obs:
        Delta3, Delta4 = iset.D_obs
        if p3['d0'] in ['n', 'n2']:
            d0mean = p1['d0']
        else:
            d0mean = np.mean([p1['d0'], p2['d0']], axis=0)
        R = (include_main_iso(d0mean)*1.e-3 + 1.) * model.R_std
        delta3 = (Delta3 + 1.e3) * R[1] * R[2] / model.R_std[3] - 1.e3
        delta4 = (Delta4 + 1.e3) * R[2] * R[2] / model.R_std[4] * 3/8 - 1.e3
        i3 = 2
        i4 = 3
        if p3['d0'] == 'n':
            p1['d0'][i3] = delta3
            p1['d0'][i4] = delta4
            p2['d0'][[i3, i4], :4] = np.abs(p1['d0'][[i3, i4], :4])* .2
            p2['d0'][[i3, i4], 4:] = np.abs(p1['d0'][[i3, i4], 4:])* .5
        else:
            p1['d0'][i3, :4] = delta3[:4] - np.abs(delta3[:4]) * .2
            p2['d0'][i3, :4] = delta3[:4] + np.abs(delta3[:4]) * .2
            p1['d0'][i3, 4:] = delta3[4:] - np.abs(delta3[4:]) * .5
            p2['d0'][i3, 4:] = delta3[4:] + np.abs(delta3[4:]) * .5
            p1['d0'][i4, :4] = delta4[:4] - np.abs(delta4[:4]) * .2
            p2['d0'][i4, :4] = delta4[:4] + np.abs(delta4[:4]) * .2
            p1['d0'][i4, 4:] = delta4[4:] - np.abs(delta4[4:]) * .5
            p2['d0'][i4, 4:] = delta4[4:] + np.abs(delta4[4:]) * .5
    # Alternate clumped signatures
    if iset.D_q:
        for k, v in iset.D_q.items():
            for n, i in enumerate(v):
                if i is None:
                    continue
                D_q[k][..., n, :] = i
            p1[k] = Delta2delta_CH4(
                    include_main_iso(p1[k], 0.), D_q[k], model.R_std
                    )[..., 1:, :]

    # Save defaults before slice
    p1_def = OrderedDict()
    p2_def = OrderedDict()
    p3_def = OrderedDict()
    for k in iset.y_var + iset.theta_var + iset.model_var:
        p1_def[k] = p1[k].copy()
        p2_def[k] = p2[k].copy()
        p3_def[k] = p3[k]
    p1.clear()
    p2.clear()
    p3.clear()
    for k in iset.y_var + iset.theta_var:
        p1[k] = p1_def[k].copy()
        p2[k] = p2_def[k].copy()
        p3[k] = p3_def[k]

    if iset.sce_run[2] == 1:
        # remove tropospheric non-OH sink
        p1['tau'][..., :8] = np.inf
        p1['KIE_tau'][..., :8] = np.inf
        set_model_var(
                model, {'tau': p1['tau']}, ptres, dates
                )
        set_model_var(
                model,
                {'KIE_tau': np.mean([p1['KIE_tau'], p2['KIE_tau']], axis=0)},
                ptres,
                dates
                )
        p1['tau'] = p1['tau'][..., 8:]
        p2['tau'] = p2['tau'][..., 8:]
    
        p2['KIE_tau'] = p2['KIE_tau'][..., 8:]
        iset.pslice['tau'] = np.s_[..., 8:]

    if iset.pslice:
        for k, v in iset.pslice.items():
            if k in iset.y_var + iset.theta_var:
                p1[k] = p1[k][v]
                p2[k] = p2[k][v]

    # Steady state
    if iset.steady:
        for k in [k for k, v in ptres.items()
                  if v not in ['ic'] and isinstance(v, str)]:
            p1[k][:] = p1[k][0]
            p2[k][:] = p2[k][0]

    # Assign multivariate normal
    for k in iset.theta_var:
        if p3[k] == 'n':
            if k in ['tv', 'vv', 'dummy']:
                p2[k] = calc_cov(p2[k], 0, 1)
            else:
                p2[k] = calc_cov(p2[k], 10, 5)
            p3[k] = 'n2'

    # Set metadata
    for k, v in pmeta.items():
        setattr(model, k, v)

    # Set model variables
    set_model_var(
            model,
            {
                k:
                p1_def[k] if p3_def[k] in ['n', 'n2'] else
                np.mean([p1_def[k], p2_def[k]], axis=0)
                for k in iset.theta_var + iset.model_var
                },
            ptres, dates, {}, iset.D_obs, D_q
            )

    return model, dates, p1, p2, p3, ptres, p1_def, p2_def, p3_def, D_q


def read_p(isce, p1, p2, p3, iset=False):
    r"""

    Parameters
    ----------
    isce : list of str
        List of scenarios.
    p1, p2, p3 : dict
        Variable dictionary to modify.
    iset : Settings or False
        Settings. If False is given, settings of the first scenario will be
        read in and used.
        
    """
    if not iset:
        iset = Settings()
        iset.read(os.path.join(isce[0], 'settings.json'))
    ifile0 = os.path.join(isce[0], 'out_theta.npz')
    ifile1 = os.path.join(isce[0], 'p1.npz')
    ifile2 = os.path.join(isce[0], 'p2.npz')
    ifile3 = os.path.join(isce[0], 'p3.npz')
    ifile4 = os.path.join(isce[0], 'p1_def.npz')
    ifile5 = os.path.join(isce[0], 'p2_def.npz')
    ifile6 = os.path.join(isce[0], 'p3_def.npz')
    arrshp = {k: v.shape for k, v in np.load(ifile1).items()}
    arrshp2 = {k: v.shape for k, v in np.load(ifile4).items()}

    idata0 = np.load(ifile0, 'r')
    for k in isce[1]:
        if k not in arrshp and k not in arrshp2:
            continue
        if k in iset.theta_var:
            v0 = np.percentile(
                    idata0[k].reshape(-1, *arrshp[k]),
                    [16, 50, 84],
                    axis=0
                    )
            if p3[k] in ['n', 'n2']:
                v1 = v0[1]
                v2 = v0[2] - v0[0]
            elif p3[k] in ['u']:
                v1 = v0[0]
                v2 = v0[2]
            v3 = p3[k]
        elif k in arrshp:
            v1 = np.load(ifile1, 'r')[k]
            v2 = np.load(ifile2, 'r')[k]
            v3 = np.load(ifile3, 'r')[k]
        else:
            v1 = np.load(ifile4, 'r')[k]
            v2 = np.load(ifile5, 'r')[k]
            v3 = np.load(ifile6, 'r')[k]
        if k not in iset.pslice:
            p1[k] = v1
            p2[k] = v2
        else:
            loc = iset.pslice[k]
            p1[k][loc] = v1
            if v3 in ['n2']:
                p2[k][loc] = np.sqrt(
                        np.diagonal(v2, axis1=-2, axis2=-1)
                        ).reshape(v1.shape)
            else:
                p2[k][loc] = v2


def construct_dq(dq, D_q, R_std, dq_ref=False, pslice=False):
    r"""

    Parameters
    ----------
    dq : ndarray
        Isotopic signature. Second last axis is for isotopes/isotopologues.
        Do not include the base/commonest species.
    D_q : ndarray or False
        Clumped signature. Second last axis is for isotopologues.
    R_std : array-like
        R_std for all isotopes/isotopologues involved.
    dq_ref : False or ndarray, optional
        Reference array determining the dimensions of the output. For each
        axis, the maximum size between dq and dq_ref will be chosen.
        No values in dq_ref will be used.
    pslice : False or slice index, optional
        Indicating which slice of dq_ref is for dq

    Returns
    -------
    odata : ndarray
        Output data.

    """
    if pslice is not False and dq_ref is not False:
        shp1 = dq_ref.shape
        shp2 = dq.shape
        ndim = max(dq_ref.ndim, dq.ndim)
        shp = np.zeros((2, ndim), np.int64)
        shp[0, ndim-dq_ref.ndim:] = np.array(shp1)
        shp[1, ndim-dq.ndim:] = np.array(shp2)
        d = np.zeros(tuple(shp.max(0)))
        d[pslice] = dq
        idata = include_main_iso(d, 0.)
    else:
        idata = include_main_iso(dq, 0.)
    if D_q is not False:
        odata = Delta2delta_CH4(idata, D_q, R_std)
    else:
        odata = idata
    return odata


def calc_gradual_weights(s, e, scale=1.):
    r"""Apply gradual weights

    For s and e with first axis size n:
        diff[i] = (e[i] - s[i])*i/n

    Parameters
    ----------
    s, e : ndarray
        Start and end arrays of broadcastable shapes.
    scale : float, optional
        Multiplication factor for weights.

    Returns
    -------
    s, diff : ndarray
        Start array, and gradaully increasing difference between s and e.

    """
    weight = np.linspace(0., 1., s.shape[0])
    diff = np.einsum('i...,i->i...', (e - s) * scale, weight)
    return s, diff


def flatten_chain(idata, ndim):
    r"""Flatten chain

    Parameters
    ----------
    idata : ndarray
        Input data.
    ndim : int
        Number of dimensions of idata to keep.

    Returns
    -------
    odata : ndarray
        Flattened array.
    """
    odata = idata.reshape(-1, *idata.shape[-ndim:])
    return odata 


def calc_D_q(p1, p2, p3, R_std):
    r"""Calculate D_q

    Parameters
    ----------
    p1, p2, p3 : dict
        PDF parameters only containing dq.
    R_std : array-like
        R_std of those in dq.

    Returns
    -------
    D_q : dict
        D_q.

    """
    d_q = calc_bounds(p1, p2, p3)[0]
    R_q = {k: (v*1.e-3 + 1.) * R_std for k, v in d_q.items()}
    i1 = np.s_[..., 0, :]
    i2 = np.s_[..., 1, :]
    i3 = np.s_[..., 2, :]
    i4 = np.s_[..., 3, :]
    D_q = {
        k:
        np.moveaxis(
            np.array([
                (v[i3]/v[i1]/v[i2] - 1.) * 1.e3,
                (v[i4]/v[i2]**2 * 8./3. - 1.) * 1.e3
                ]),
            0, -2
            )
        }
    return D_q


def get_plot_data(
        isets, y_vars, theta_vars, model_vars,
        cs, ys, thetas, constant, D_q,
        R_std, M, boxes, global_q=True
        ):
    r"""Hard Coded Plot Data Generator
    
    Parameters
    ----------
    isets : dict
        {pkey: Settings}
        Input settings. pkey is the key/name of that settings.
    y_vars, theta_vars, model_vars : dict
        {pkey: list of str}
        Variables in y and theta vectors and model constants.
    cs, ys, thetas, constant : dict
        {pkey: ndarray} for cs
        {pkey: {variable: ndarray}} for y and theta and model constants.
        Mixing ratios of species, y and theta vectors.
        pkey should only exist in either cs or ys.
        thetas should contain all pkeys.
    D_q : dict
        {pkey: {dq_sector: ndarray}}
        Clumped isotopic signature for emsisions.
        If False, D_q for individual scenarios will be obtained.
    R_std, M : array-like
        R_std and molecular mass of species.
    boxes : list of int, optional
        Boxes to include.
    global_q : bool, optional
        If true, output global emissions instead of individual boxes.

    Returns
    -------
    plot_data : dict
        Plot data.

    """
    # y vector
    y = {}
    plot_y = {}
    for n, v in cs.items():
        if v is None:
            continue
        if isinstance(v, bool) and v:
            # from file
            v = np.load(os.path.join(isets[n].odir, 'out_c.npy'), 'r')
        y[n] = calc_y(
                y_vars[n],
                ['C12H4', 'C13H4', 'C12H3D', 'C13H3D', 'C12H2D2'],
                v,
                isets[n].iso_sel
                )
    for n, v in ys.items():
        if isinstance(v, list):
            y[n] = list(calc_bounds(*v))
        else:
            y[n] = v
    plot_y = {
            k: {
                n:
                np.array([v[1][k], v[0][k], v[2][k]])[..., boxes]
                if isinstance(v, list) else
                v[k][..., boxes]
                if v[k].ndim == 3 and v[k].shape[0] == 1 else
                np.percentile(
                    flatten_chain(v[k], 2)[..., boxes],
                    [16, 50, 84],
                    axis=0)
                if v[k].ndim > 2 else
                v[k][np.newaxis, ..., boxes]
                for n, v in y.items()
                }
            for k in ['c', 'd_13C', 'd_2H', 'D_C13H3D', 'D_C12H2D2']
            }

    # theta vector
    plot_var = ['OH_a', 'q', 'q_sum', 'dq_13CH4', 'dq_12CH3D']
    plot_theta = OrderedDict([(k, OrderedDict()) for k in plot_var])
    dq_def = {}
    for n in constant:
        sectors_d = [
                k for k in theta_vars[n]
                if k.startswith('dq_') and thetas[n] is not None
                ]
        if isinstance(constant[n], list):
            dq_def[n] = calc_bounds(
                {k: v for k, v in constant[n][0].items() if k in sectors_d},
                {k: v for k, v in constant[n][1].items() if k in sectors_d},
                {k: v for k, v in constant[n][2].items() if k in sectors_d},
                )[0]
        else:
            dq_def[n] = {k: v for k, v in constant[n] if k in sectors_d},
    for n, v0 in thetas.items():
        if v0 is None:
            continue
        elif isinstance(v0, bool) and v0:
            v = {
                    k: v 
                    for k, v in np.load(
                        os.path.join(isets[n].odir, 'out_theta.npz'), 'r'
                        ).items()
                    }
        elif isinstance(v0, bool) and not v0:
            v = {}
        elif isinstance(v0, list):
            val = calc_bounds(*v0)
            v = {
                    k:
                    np.array([val[1][k], val[0][k], val[2][k]])
                    for k in v0[0]
                    }
        elif isinstance(v0, np.lib.npyio.NpzFile):
            v = {k: v for k, v in v0.items()}
        else:
            v = v0.copy()
        if n in model_vars and n not in constant:
            ifile1 = os.path.join(isets[n].odir, 'p1_def.npz')
            ifile2 = os.path.join(isets[n].odir, 'p2_def.npz')
            ifile3 = os.path.join(isets[n].odir, 'p3_def.npz')
            v.update(
                    {
                        k:
                        v[isets[n].pslice[k]] if k in isets[n].pslice else
                        v
                        for k, v in 
                        calc_bounds(
                            np.load(ifile1, 'r'),
                            np.load(ifile2, 'r'),
                            np.load(ifile3, 'r'),
                            )[0].items()
                        if k in model_vars[n]
                        }
                    )
        elif n in model_vars and isinstance(constant[n], list):
            v.update({
                k: v for k, v in calc_bounds(*constant[n])[0].items()
                if k in model_vars[n]
                })
        elif n in model_vars:
            v.update({
                k: v for k, v in constant[n].items() if k in model_vars[n]
                })
        # OH_a
        k = 'OH_a'
        ndim = np.load(os.path.join(isets[n].odir, 'p1_def.npz'), 'r')[k].ndim
        if k not in theta_vars[n]:
            plot_theta[k][n] = v[k][np.newaxis]
        elif isinstance(thetas[n], list):
            plot_theta[k][n] = np.array(v[k])
        elif v[k].ndim == ndim:
            plot_theta[k][n] = v[k][np.newaxis]
        elif v[k].ndim == ndim + 1:
            plot_theta[k][n] = v[k]
        elif v[k].ndim > ndim:
            plot_theta[k][n] = np.percentile(v[k], [16, 50, 84], axis=(0, 1))
        else:
            plot_theta[k][n] = v[k]
        # q
        k = 'q'
        ivar = []
        if n in theta_vars:
            ivar += theta_vars[n]
        if n in model_vars:
            ivar += model_vars[n]
        sectors_q = [k for k in ivar if k.startswith('q')]
        q = flatten_chain(
                sum([np.array(v[s]) for s in sectors_q])[..., 0, :],
                2
                )
        ndim = np.load(
                os.path.join(isets[n].odir, 'p1_def.npz'), 'r'
                )[sectors_q[0]].ndim
        if not any([s in theta_vars[n] for s in sectors_q]):
            plot_theta[k][n] = q
        elif isinstance(thetas[n], list):
            plot_theta[k][n] = q
        elif q.shape[0] == 1:
            plot_theta[k][n] = q
        else:
            plot_theta[k][n] = np.percentile(q, [16, 50, 84], axis=0)
        # q_sum
        k = 'q_sum'
        ivar = []
        if n in theta_vars:
            ivar += theta_vars[n]
        if n in model_vars:
            ivar += model_vars[n]
        q_sum = flatten_chain(
                sum([np.array(v[s]) for s in sectors_q]).sum(-1), 2
                )
        if not any([s in theta_vars[n] for s in sectors_q]):
            plot_theta[k][n] = q_sum
        elif isinstance(thetas[n], list):
            plot_theta[k][n] = q_sum
        elif q_sum.shape[0] == 1:
            plot_theta[k][n] = q_sum
        else:
            plot_theta[k][n] = np.percentile(q_sum, [16, 50, 84], axis=0)
        # dq
        k = 'dq'
        sectors_d = [k for k in ivar if k.startswith('dq')]
        dq = combine_dq(
                {
                    s:
                    v[s]
                    if isinstance(v[s], np.ndarray) else
                    v[s][1]
                    for s in sectors_q
                    },
                {
                    s:
                    construct_dq(
                        v[s], False, R_std, False, False
                        )
                    if n not in D_q and s not in D_q[n] else
                    construct_dq(
                        v[s], D_q[n][s], R_std,
                        dq_def[n][s], isets[n].pslice[s]
                        )
                    if s in isets[n].pslice and s in dq_def[n] else
                    construct_dq(
                        v[s], D_q[n][s], R_std, False, False
                        )
                    if s in D_q[n] and v[s].ndim == D_q[n][s].ndim else 
                    construct_dq(
                        v[s], False, R_std, False, False
                        )
                    for s in sectors_d
                    },
                R_std, M, isets[n].iso_sel
                )
        for ni, ki in enumerate(['dq_13CH4', 'dq_12CH3D']):
            if not any([s in theta_vars[n] for s in sectors_d]):
                plot_theta[ki][n] = dq[np.newaxis, ..., ni+1, boxes]
            elif isinstance(thetas[n], list):
                plot_theta[ki][n] = dq[..., ni+1, boxes]
            elif dq.ndim == 3:
                plot_theta[ki][n] = dq[np.newaxis, ..., ni+1, boxes]
            elif dq.ndim == 4 and dq.shape[0] == 1:
                plot_theta[ki][n] = dq[..., ni+1, boxes]
            else:
                plot_theta[ki][n] = np.percentile(
                        flatten_chain(dq[..., ni+1, boxes], 2),
                        [16, 50, 84],
                        axis=0
                        )
    plot_data = {**plot_y, **plot_theta}

    return plot_data


def get_plot_data_file(idir, boxes, global_q):
    r"""Wrapper function for get_plot_data for getting two model outputs.

    Parameters
    ----------
    idir :path-like
        Input directory, which are model output directory.
    boxes : list of int, optional
        Boxes to include.
    global_q : bool, optional
        If true, output global emissions instead of individual boxes.

    Returns
    -------
    plot_data : dict
        Plot data.

    """
    iset = io_r_settings(os.path.join(idir, 'settings.json'))
    (
        model, dates, p1, p2, p3, ptres, p1_def, p2_def, p3_def, D_q,
        ) = prepare_scenario(iset)
    c0 = model.run(iset.iso_sel)['c'][..., :4]
    out_c = np.load(os.path.join(idir, 'out_c.npy'), 'r')
    out_theta = np.load(os.path.join(idir, 'out_theta.npz'), 'r')
    plot_data = get_plot_data(
            isets={
                'model0': iset,
                'model': iset,
                'obs': iset
                },
            y_vars={
                'model0': iset.y_var,
                'model': iset.y_var,
                'obs': iset.y_var
                },
            theta_vars={
                'model0': iset.theta_var,
                'model': iset.theta_var
                },
            model_vars={
                'model0': iset.model_var,
                'model': iset.model_var
                },
            cs={
                'model0': c0,
                'model': out_c
                },
            ys={
                'obs': [p1, p2, p3]
                },
            thetas={
                'model0': [p1, p2, p3],
                'model': out_theta
                },
            constant={
                'model0': [
                    p1_def, p2_def, p3_def
                    ],
                'model': [
                    p1_def, p2_def, p3_def
                    ]
                },
            D_q={
                'model0': D_q,
                'model': D_q
                },
            R_std = model.R_std,
            M = model.mol_m,
            boxes=boxes,
            global_q=global_q
            )
    return plot_data


def get_plot_data_compare(
        idirs, model, D_q, y0=None, theta0=None, boxes=[0,3], global_q=True
        ):
    r"""Wrapper function for get_plot_data for getting two model outputs.

    Parameters
    ----------
    idirs : dict
        {scenario_keys: path-like}
        Input directories, which are model output directories.
        To have y0 or theta0 to be None or dict, idirs, keys must include "0",
        considered as the reference.
    model : Model
        Base Model. Only required for its attriubutes R_std, mol_m and iso.
    D_q : dict or False
        {dq_sector: ndarray}
        D_q common to all scenario_keys.
        If False, D_q for individual scenarios will be obtained.
    y0, theta0 : None, False or dict, optional
        y/theta vector for reference scenario.
    boxes : list of int, optional
        Boxes to include.
    global_q : bool, optional
        If true, output global emissions instead of individual boxes.

    Returns
    -------
    plot_data : dict
        Plot data.

    """
    isets = {
            m: io_r_settings(os.path.join(idir, 'settings.json'))
            for m, idir in idirs.items()
            }
    if D_q is False:
        sector_d = {
            m: [
                k
                for k in np.load(os.path.join(idir, 'p3_def.npz'), 'r').keys()
                if k.startswith('dq_')
                ]
            for m, idir in idirs.items()
            }
        D_q_in = {
            m: calc_D_q(
                {
                    k: np.load(np.load(os.path.join(idir, 'p1_def.npz')), 'r')
                    for k in sector_d[m]
                    },
                {
                    k: np.load(np.load(os.path.join(idir, 'p2_def.npz')), 'r')
                    for k in sector_d[m]
                    },
                {
                    k: np.load(np.load(os.path.join(idir, 'p3_def.npz')), 'r')
                    for k in sector_d[m]
                    },
                model.R_std[1:]
                )
            for m, idir in idirs.items()
            }
    else:
        D_q_in = {m: D_q for m in isets}
    if y0 is None:
        y0 = {
                k:
                np.percentile(v, [50], axis=(0, 1))
                for k, v in
                calc_y(
                    isets[0].y_var,
                    model.iso, 
                    np.load(os.path.join(isets[0].odir, 'out_c.npy'), 'r'),
                    isets[0].iso_sel
                    ).items()
                }
    if theta0 is None:
        theta0 = {
                k:
                np.percentile(v, [50], axis=(0, 1))
                for k, v in
                np.load(
                    os.path.join(isets[0].odir, 'out_theta.npz'), 'r'
                    ).items()
                }
    plot_data = get_plot_data(
            isets=isets,
            y_vars={
                k: v.y_var for k, v in isets.items()
                },
            theta_vars={
                k: v.theta_var for k, v in isets.items()
                },
            model_vars={
                k: v.model_var for k, v in isets.items()
                },
            cs={
                k: np.load(os.path.join(v.odir, 'out_c.npy'), 'r')
                for k, v in isets.items() if k != 0
                },
            ys={0: y0} if y0 else {},
            thetas={
                **({0: theta0} if theta0 else {}),
                **{
                    k:
                    np.load(os.path.join(v.odir, 'out_theta.npz'), 'r')
                    if os.path.exists(os.path.join(v.odir, 'out_theta.npz'))
                    else
                    np.load(os.path.join(v.odir, 'p1.npz'), 'r')
                    if os.path.exists(os.path.join(v.odir, 'p1.npz')) else
                    False
                    for k, v in isets.items()
                    if k != 0
                    }
                },
            constant={
                k:
                [
                    np.load(os.path.join(v.odir, 'p1_def.npz'), 'r'),
                    np.load(os.path.join(v.odir, 'p2_def.npz'), 'r'),
                    np.load(os.path.join(v.odir, 'p3_def.npz'), 'r')
                    ]
                for k, v in isets.items()
                },
            D_q=D_q_in,
            R_std = model.R_std,
            M = model.mol_m,
            boxes=boxes,
            global_q=global_q
            )
    return plot_data

