# -*- coding: utf-8 -*-
r"""
mcmc_utils.py

Markov Chain Monte Carlo functions.

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20180710    EC  Initial code.
20200822    EC  Packaged.
"""

# Standard Library imports
import numpy as np

# Third party imports
from collections import OrderedDict

# Local imports
from chung_box_model.calc import Delta2delta_CH4


def flatten_arrays(idata):
    r"""Create 1darray from dictionary

    Use OrderedDict to preserve key/variable order.

    Parameters
    ----------
    idata : OrderedDict
        Input data. {key, ndarray}

    Returns
    -------
    odata : ndarray
        Input data flattened to 1d in the given order.
    dim : OrderedDict
        Dimensions of original data.

    """
    dim = OrderedDict((k, v.shape) for k, v in idata.items())
    odata = np.full(sum([np.prod(v) for v in dim.values()]), np.nan)
    idx0 = 0
    for k, v in dim.items():
        idx1 = np.prod(v)
        odata[idx0:idx0+idx1] = idata[k].flatten()
        idx0 += idx1
    return odata, dim


def expand_arrays(idata, dim):
    r"""Unflatten 1darray to dictionary

    Use OrderedDict to preserve key/variable order.

    Parameters
    ----------
    idata : ndarray
        Input data flattened to 1d in the given order.
    dim : OrderedDict
        Dimensions of original data. {key, tuple-like}

    Returns
    -------
    odata : OrderedDict
        Input data. {key, ndarray}

    """
    odata = {}
    shape = idata.shape
    idx0 = 0
    for k, v in dim.items():
        idx1 = np.prod(v)
        odata[k] = idata[..., idx0:idx0+idx1].reshape((*shape[:-1], *dim[k]))
        idx0 += idx1
    return odata


def perturb_arrays_flat(p1, p2, p3, dim, ensemble=1):
    r"""Perturb data given distribution parameters

    Applies distribution function based on type (p3) for a given size (dim).

    Parameters
    ----------
    p1, p2 : ndarray
        1d.
        Distribution parameters. For this function, both need to be flattened.
                        p1      p2          p3
            normal      mean    std dev     'n'
            uniform     lower   upper bound 'u'
    p3, dim : dict
        Distribution type and dimesion of original vector.
    ensemble : int
        Number of perturbations.

    Returns
    -------
    odata : ndarray
        ensemble, *p1.shape
        Perturbed vector.

    """
    odata = np.zeros((ensemble, *p1.shape))
    i0 = 0  # for p1
    j0 = 0  # for p2
    for k, v in dim.items():
        i1 = i0 + np.prod(v)
        if p3[k] == 'n':
            j1 = j0 + np.prod(v)
            odata[:, i0:i1] = np.random.normal(
                    p1[i0:i1],
                    p2[j0:j1],
                    (ensemble, i1 - i0)
                    )
        elif p3[k] == 'n2':
            l = np.prod(v)
            j1 = j0 + l**2
            odata[:, i0:i1] = np.random.multivariate_normal(
                    p1[i0:i1],
                    p2[j0:j1].reshape(l, l),
                    ensemble
                    )
        elif p3[k] == 'u':
            j1 = j0 + np.prod(v)
            odata[:, i0:i1] = np.random.uniform(
                    p1[i0:i1],
                    p2[j0:j1],
                    (ensemble, i1 - i0)
                    )
        i0 = i1
        j0 = j1
    return odata


def calc_cov(sigma, t, tcorr):
    r"""Covariance matrix from standard deviation with temporal correlation

    cov_{i,j} = sigma_i * sigma_j * exp(-(j-i) / tcorr)
        
        where t = max(j - i)

    Modified from:
        Wang, Broquet and Ciais et al. 2018.
            Atmos. Chem. Phys., 18, 4229-4250
            https://doi.org/10.5194/acp-18-4229-2018

    Parameters
    ----------
    sigma : array_like
        Standard deviation.
    t : int
        Number of unit time to apply temporal correlation.
    tcorr : int
        Temporal autocorrelation in unit time.

    Returns
    -------
    out : array_like
        Output data. Size is sigma.size x sigma.size.

    """
    base = sigma.flatten()
    i, b = np.atleast_3d(sigma).shape[-2:]
    out = np.zeros((base.size, base.size))
    for n in range(t+1):
        idx = n*i*b
        value = base[0:-idx or None]*base[idx:]*np.exp(-n/tcorr)
        np.fill_diagonal(out[idx:], value)
        np.fill_diagonal(out[:, idx:], value)
    return out


def include_main_iso(idata, value=0.):
    r"""Insert value for the base isotopologue

    Parameters
    ----------
    idata : ndarray
        Input data that lacks information of common isotope/isotopologue.
        Isotope axis should be at the second last dimension.
    value : float, optional
        Defaults to 0.

    Returns
    -------
    odata : ndarray
        Output data.

    """
    shp = list(idata.shape)
    shp[-2] += 1
    odata = np.full(shp, value)
    odata[..., 1:, :] = idata
    return odata


def update_iso_idx(idata, base, idx):
    r"""Selectively update isotopologues using indices.

    Parameters
    ----------
    idata : array_like
        Input data.
    base : array_like
        Same shape as idata, apart from -2 axis,
            where idata.shape[-2] <= base.shape[-2].
    idx : float or array_like
        Selection in the -2 dimension.

    Returns
    -------
    odata : array_like
        Output data. Has same shape as base.

    """
    odata = base.copy()
    odata[..., idx, :] = idata
    return odata


def update_slice(idata, base, pslice):
    r"""Selectively update isotopologues using slices.

    Parameters
    ----------
    idata : array_like
        Input data.
    base : array_like
        Sizes of each dimensions should be larger than that of idata.
    pslice : tuple or list
        Slice parameter.

    Returns
    -------
    odata : array_like
        Output data. Has same shape as base.

    """
    odata = base.copy()
    odata[pslice] = idata
    return odata


def calc_bounds(p1, p2, p3):
    r"""Calculate mean and outerbounds

    Parameters
    ----------
    p1, p2, p3 : dict
        Distribution parameters. {key: ndarray}
                        p1      p2          p3
            normal      mean    std dev     'n'
            uniform     lower   upper bound 'u'
            m. normal   mean    covariance  'n2'

    Returns
    -------
    mean, lower, upper : dict
        Mean and outer bounds. {key: ndarray}

    """
    mean = OrderedDict()
    lower = OrderedDict()
    upper = OrderedDict()
    for k, v in p3.items():
        if v == 'n':
            mean[k] = p1[k]
            lower[k] = p1[k] - p2[k]
            upper[k] = p1[k] + p2[k]
        elif v == 'n2':
            sigma = np.sqrt(np.diag(p2[k]))
            mean[k] = p1[k]
            lower[k] = p1[k] - sigma.reshape(p1[k].shape)
            upper[k] = p1[k] + sigma.reshape(p1[k].shape)
        elif v == 'u':
            mean[k] = np.mean([p1[k], p2[k]], axis=0)
            lower[k] = p1[k]
            upper[k] = p2[k]
    return mean, lower, upper


def calc_pdf_params(mean, value, pdf='n', method='m'):
    r"""Calculate p1, p2 from mean and value depending on pdf and method.
    
    If method == 'm':
        pdf     p1          p2
        n       mean        mean x value
        u       mean -+ |mean x value|
    If method == 'a':
        pdf     p1          p2
        n       mean        value
        u       mean -+ |value|
    
    Parameters
    ----------
    mean : ndarray
        Mean.
    value : ndarray or float
        Absolute/relative offsets.
    pdf : str {'n', 'u'}
        PDF. Normal or Uniform distributions are supported.
    method : str {'m', 'a'}
        Determines if value is a multiplication factor or additive.

    Returns
    -------
    p1, p2 : ndarray
        PDF parameters.
    """
    if method == 'm':
        if pdf == 'u':
            p1, p2 = mean - np.abs(mean * value), mean + np.abs(mean * value)
        elif pdf == 'n':
            p1, p2 = mean, np.abs(mean * value)
    elif method == 'a':
        if pdf == 'u':
            p1, p2 = mean - value, mean + value
        elif pdf == 'n':
            p1 = mean
            p2 = (
                np.full(mean.shape, value)
                if isinstance(value, float) else
                value
                )
    else:
        raise Exception("method is either 'm' or 'a'")
    return p1, p2


def theta2input(date1, date2, idata):
    r"""Tile theta to monthlies and forward fill NaNs along first axis

    date1, date2 : list-like
        Date objects for y and theta, respectively.
    """
    odata = np.full((date1.size, *idata.shape[1:]), np.nan)
    odata[np.isin(date1, date2)] = idata
    mask = np.isnan(odata)
    idx = np.where(
            ~mask,
            np.tile(np.arange(date1.size).reshape(date1.size,
                                                  *[1]*(odata.ndim-1)),
                    (1, *odata.shape[1:])),
            0)
    odata[mask] = odata[(np.maximum.accumulate(idx, axis=0)[mask],
                         *np.nonzero(mask)[1:])]
    return odata


def ln_like(x, p1, p2, pdf='n'):
    r"""Calculate log of likelihood

    Parameters
    ----------
    x, p1, p2 : float or ndarray
        Value and parameters.
                    p1      p2
        normal      mean    standard deviation
        m. normal   mean    covariance matrix
        uniform     lower   upper bound

        All values should be finite if pdf is "n2"
    pdf : {'n', 'n2'. 'u'}
        Type of probability density function.

    Returns
    ------
    L : float
        Log likelihood

    """
    if ~np.isfinite(x).all():
        return -np.inf
    if pdf == 'n':
        # exlude invalid values
        valid = np.isfinite(p1) & np.isfinite(p2) & np.not_equal(p2, 0.)
        # number of True indices
        n = valid.sum()
        # PDF
        L = (-0.5 * n * np.log(2.0 * np.pi) - 
             (np.log(p2[valid]) +
              0.5 * ((x[valid] - p1[valid]) / p2[valid])**2
              ).sum()
             )
    elif pdf == 'n2':
        n = x.size
        mis = (x - p1).flatten()
        L = (-0.5 * n * np.log(2.0 * np.pi) +
             -0.5 * np.linalg.slogdet(p2)[1] +
             -0.5 * np.dot(mis.T, np.dot(np.linalg.inv(p2), mis))
             )
    elif pdf == 'u':
        valid = np.isfinite(p2 - p1) & np.not_equal(p1, p2)
        if np.logical_or(x[valid] < p1[valid], x[valid] > p2[valid]).any():
            return -np.inf
        L = -np.log(p2[valid] - p1[valid]).sum()
    return L

