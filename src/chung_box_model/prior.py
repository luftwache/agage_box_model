# -*- coding: utf-8 -*-
r"""
prior.py

Process prior data

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20200822    EC  Packaged.
"""
# Standard Library imports
import h5py
import linecache
import netCDF4
import numpy as np
import pandas as pd
import scipy.io
import os
import re


def io_r_caos_csv(ifile):
    r"""Read CAOS data.

    Parameter
    ---------
    ifile : path-like
        Input file.

    Returns
    -------
    odata : DataFrame
        Output data.

    """
    with open(ifile) as f:
        header = ([l for l in f.readlines() if re.match('^#', l)][-1]
                  .strip('#').strip().split('\t')
                  )
    idata = pd.read_csv(ifile,
                        sep='\t+',
                        skipinitialspace=True,
                        comment='#',
                        header=None,
                        names=header,
                        engine='python')
    dt_index = [k for k in ['year', 'month', 'day', 'hour', 'minutes']
                if k in header]
    value_no = len(header) - len(dt_index) - 1
    new_header = [*['value{}'.format(i) for i in range(value_no)], 'flag']
    idata.index = pd.to_datetime(
            idata[dt_index]
            )
    odata = idata.iloc[:, -len(new_header):]
    odata.columns = new_header
    return odata


def calc_cell_area(lat, hres):
    r"""Calculate area of a gridcell

    A = r^2 * lon * d(sin(lat))

    Parameters
    ----------
    lat ndarray
        Latitudes.
    hres : float
        Logitude.

    Returns
    -------
    out : ndarray
        Output data.

    """
    if (lat[0] > lat[1]).all():
        h = np.deg2rad(lat[0])
        l = np.deg2rad(lat[1])
    else:
        h = np.deg2rad(lat[1])
        l = np.deg2rad(lat[0])
    out = 6371.0e3**2.0 * np.deg2rad(hres) * (np.sin(h) - np.sin(l))
    # out = (6371.0e3**2.0 * np.deg2rad(res) *
    #        2.0 * np.cos(np.deg2rad(lat)) * np.sin(np.deg2rad(0.5*res)
    #        )
    return out


def get_data(name, dpath, cat1=None, cat2=None):
    r"""Read binary prior data

    Parameters
    ----------
    name : str {'agage', 'noaa', 'edgar_txt', 'gfed', 'wetcharts'}
        Name of dataset.
    dpath : path-like
        Directory path of input files.
    cat1, cat2 : str
        Depends on dataset.

    Returns
    -------
    odata :
        Format depends on dataset.

    """
    if name == 'agage':
        if cat1 == None:
            cat1 = 'CH4'  # default to methane
        meta = {'cgo': {'header': 13, 'CH4': 22},
                'mhd': {'header': 13, 'CH4': 22},
                'rpb': {'header': 13, 'CH4': 22},
                'smo': {'header': 13, 'CH4': 22},
                'thd': {'header': 13, 'CH4': 22}
                }
        odata = pd.DataFrame({})
        for site, finfo in meta.items():
            f = os.path.join(dpath, '{}-gcmd_mon.csv'.format(site.upper()))
            col = finfo[cat1]
            obs = pd.read_csv(f,
                              header=finfo['header'],
                              usecols=[1, 2, col-1, col, col+1],
                              parse_dates={'date': [0, 1]},
                              index_col=0,
                              skipinitialspace=True)
            for col_name in obs:
                odata['{}_{}'.format(site, re.sub('[ \.]', '', col_name))] =\
                    obs[col_name]

    elif name == 'noaa':
        odata = pd.DataFrame({})
        regex = r'{}_[a-z]{{3}}.*month\.txt'.format(cat2)
        files = [f for f in os.listdir(dpath) if re.fullmatch(regex, f)]
        for f in files:
            fpath = os.path.join(dpath, f)
            header_no = int(linecache.getline(fpath, 1).split(':')[1].strip())
            code = re.match(r'^([a-z0-9]+)_([a-z0-9]+)', f)[2]
            col_name = re.sub('# data_fields: ', '',
                              linecache.getline(fpath, header_no))
            idata = pd.io.parsers.read_csv(
                            fpath, sep=r'\s+', skiprows=header_no,
                            header=None,
                            names=[c.strip() for c in col_name.split(' ')])
            idata.set_index(pd.to_datetime(idata['year']*100 +
                                           idata['month'],
                                           format='%Y%m'),
                            inplace=True)
            odata = pd.concat(
                        [odata, idata.rename(columns={'value': code})[code]],
                        axis=1,
                        sort=True)
        date = pd.date_range(odata.index[0], odata.index[-1], freq='MS')
        odata.reindex(date, fill_value=np.nan)

    elif name == 'edgar_txt':  # tonnes per unit area per year
        regex = r'.*{}_([0-9]{{4}})_IPCC_({}).*txt'.format(cat1, cat2)
        files = [f for f in os.listdir(dpath) if re.fullmatch(regex, f)]
        if not files:
            raise Exception("No files matching cat2")
        odata = {}
        for f in files:
            print(f)
            keys = re.fullmatch(regex, f)
            year = keys[1]
            idata = pd.read_csv(os.path.join(dpath, f),
                                sep=';', header=2).set_index(['lat', 'lon'])
            if year not in odata.keys():
                odata[year] = idata
            else:
                odata[year] = odata[year].add(idata, fill_value=0.)
    elif name == 'edgar_nc':  # kg m^{-2 s{-1}}
        regex = r'.*{}_([0-9]{{4}})_IPCC_({}).*nc'.format(cat1, cat2)
        files = [f for f in os.listdir(dpath) if re.fullmatch(regex, f)]

    elif name == 'gfed':  # kg m^{-2} month^{-1}
        regex = r'GFED4\.1s_([0-9]{4})\.hdf5'
        odata = {}
        if cat1 == 'CH4':
            ratio = {'SAVA': 1.94e-3,
                     'BORF': 5.96e-3,
                     'TEMF': 3.36e-3,
                     'DEFO': 5.07e-3,
                     'PEAT': 20.8e-3,
                     'AGRI': 5.82e-3}
        else:
            ratio = {'SAVA': 1.0,
                     'BORF': 1.0,
                     'TEMF': 1.0,
                     'DEFO': 1.0,
                     'PEAT': 1.0,
                     'AGRI': 1.0}
        if cat2 is not None and cat2 in ratio:
            ratio = {cat2: ratio[cat2]}
        files = [f for f in os.listdir(dpath) if re.fullmatch(regex, f)]
        lat = h5py.File(os.path.join(dpath, files[0]), 'r')['lat']
        dim = lat.shape
        odata['lat'] = lat[:].astype(np.float64)
        for f in files:
            print(f)
            keys = re.fullmatch(regex, f)
            year = keys[1]
            with h5py.File(os.path.join(dpath, f), 'r') as idata:
                for month in range(1, 13):
                    q = np.zeros(dim)
                    monthly = idata['emissions/{:02d}'.format(month)]
                    total = monthly['DM'][:].astype(np.float64)
                    for src, species_ratio in ratio.items():
                        partition = 'partitioning/DM_{}'.format(src)
                        src_ratio = monthly[partition][:].astype(np.float64)
                        q += total * src_ratio * species_ratio
                    odata['{0}-{1:02d}'.format(year, month)] = q
    elif name == 'wetcharts':
        ifile = os.path.join(dpath, 'WetCHARTs_extended_ensemble.nc4')
        idata = netCDF4.Dataset(ifile, 'r')
        odata = {}
        v = idata.variables['wetland_CH4_emissions'][:]
        time = idata.variables['time'][:]
        for t in range(len(time)):
            k = '{:04d}-{:02d}'.format(2001 + int(t)//12, int(t)%12 + 1)
            odata[k] = np.mean(v[:, t], axis=0)
        odata['lat'] = np.tile(np.vstack(idata.variables['lat'][:]), (1, 720))
        idata.close()
    return odata


def grid2box(idata, lat, factor30deg=[0.0, 0.0]):
    r"""Aggregate gridded data to semi-hemispheres

    Parameters
    ----------
    idata : ndarray
        Input gridded data
    lat : ndarray
        Latitudes of the grid
    factor30deg : array-like
        Factors to multiply for the grid cells at 30 deg N/S.
        First value applies for extratropics and the second to tropics.

    Returns
    -------
    odata : ndarray
        Output data

    """
    odata = pd.Series(np.zeros(4),
                      index=['box_{}'.format(i) for i in range(4)])
    odata['box_0'] += np.nansum(idata[(lat > 30.0)])
    odata['box_1'] += np.nansum(idata[(lat < 30.0) & (lat > 0.0)])
    odata['box_2'] += np.nansum(idata[(lat < 0.0) & (lat > -30.0)])
    odata['box_3'] += np.nansum(idata[(lat < -30.0)])
    # intersections
    if np.isin(30.0, lat):
        odata['box_0'] += np.nansum(idata[(lat == 30.0)]) * factor30deg[0]
        odata['box_1'] += np.nansum(idata[(lat == 30.0)]) * factor30deg[1]
    if np.isin(0.0, lat):
        odata['box_1'] += np.nansum(idata[(lat == 0.0)]) * 0.5
        odata['box_2'] += np.nansum(idata[(lat == 0.0)]) * 0.5
    if np.isin(-30.0, lat):
        odata['box_2'] += np.nansum(idata[(lat == -30.0)]) * factor30deg[1]
        odata['box_3'] += np.nansum(idata[(lat == -30.0)]) * factor30deg[0]
    return odata


def get_edgar(dpath, cat1, cat2):
    r"""Read and aggregate EDGAR 4.3.2 data from text files.

    Parameters
    ----------
    dpath : path-like
        Directory containing EDGAR files
    cat1, cat2 : str
        cat1 is the '{version}_CH4' and cat2 is the IPCC catetories.

    Returns
    -------
    odata : ndarray
        Output data

    """
    regex = r'.*{}_([0-9]{{4}})_IPCC_({}).*txt'.format(cat1, cat2)
    files = [f for f in os.listdir(dpath) if re.fullmatch(regex, f)]
    cols = ['box_{}'.format(i) for i in range(4)]
    odata = pd.DataFrame({}, columns=cols)
    res = 0.1
    factor30deg = (
            np.array([
                np.sin(np.deg2rad(30+res/2)) - 0.5,
                0.5 - np.sin(np.deg2rad(30-res/2))
                ]) /
            (2*np.sqrt(3)/2*np.sin(np.deg2rad(res/2)))
            )
    for f in files:
        print(f)
        keys = re.fullmatch(regex, f)
        year = pd.to_datetime(keys[1], format='%Y')
        if year not in odata.index:
            odata.loc[year] = pd.Series(np.zeros(4), index=cols)
        idata = pd.read_csv(os.path.join(dpath, f), sep=';', header=2)
        odata.loc[year] += \
            grid2box(idata['emission {} (tons)'.format(keys[1])],
                     idata['lat'],
                     factor30deg)
    return odata


def get_cmip6_input(dpath, cat1='methane'):
    r"""Read CMIP6 input data and aggregate to semi-hemispheres

    cat1 is "methane"
    hardcoded dates due to orignal data not accounting for leap years

    Parameters
    ----------
    dpath : path-like
        Directory path of input files.
    cat1 : str {'methane'}

    Returns
    -------
    odata : ndarray
        Output data.

    """
    ifile = os.path.join(dpath, 'cmip6_ch4_15x360.nc')
    idata = netCDF4.Dataset(ifile, 'r')
    # Convert "days since" to datetime
    t0 = np.abs(idata['time_bnds'][:, 0] - 0.).argmin()
    l = idata['time'].size
    date_stamp = (np.datetime64('1850-01') +
                  np.arange(-t0, l-t0, 1).astype('timedelta64[M]')
                  ).astype('datetime64[D]')
    # Check lat
    lat = idata['lat'][:]
    lat_bnds = idata['lat_bnds'][:]
    if not all(i in lat_bnds for i in [-30., 0., 30.]):
        raise Exception('Check latitudes')
    # Remove 0th year since everything other than np.datetime64 complains
    sel = date_stamp >= np.datetime64('0001')
    dates = pd.period_range(pd.Period(str(date_stamp[sel][0]), freq='M'),
                            pd.Period(str(date_stamp[sel][-1]), freq='M'),
                            freq='M')
    conc = idata[cat1][sel]
    # Area weight
    area_w = np.abs(np.diff(np.sin(np.deg2rad(lat_bnds)), axis=1))
    # Output
    odata = pd.DataFrame(np.nan, index=dates,
                         columns=['box_{}'.format(i) for i in range(4)]
                         )
    # 0.5^-1 from sin(90)-sin(30) (extratropics) and sin(30)-sin(0) (tropics)
    odata['box_0'] = 2*np.dot(conc[:, (lat>30)],
                              area_w[(lat>30)])
    odata['box_1'] = 2*np.dot(conc[:, (lat<30) & (lat>0)],
                              area_w[(lat<30) & (lat>0)])
    odata['box_2'] = 2*np.dot(conc[:, (lat<0) & (lat>-30)],
                              area_w[(lat<0) & (lat>-30)])
    odata['box_3'] = 2*np.dot(conc[:, (lat<-30)],
                              area_w[(lat<-30)])
    return odata


def create_prior(name, dpath, cat1, cat2):
    r"""Read data and aggregate to semi-hemispheres

    Parameters
    ----------
    name : str {'agage', 'noaa', 'edgar_txt', 'gfed', 'wetcharts'}
        Name of dataset.
    dpath : path-like
        Directory path of input files.
    cat1, cat2 : str
        Depends on dataset.

    Returns
    -------
    odata :
        Format depends on dataset.

    """
    idata = get_data(name, dpath, cat1, cat2)

    if name == 'agage':
        odata = pd.DataFrame({}, index=idata.index)
        meta = {'box_0': ['{}_{}'.format(loc, cat1)
                          for loc in ['mhd', 'thd']],
                'box_1': ['rpb_{}'.format(cat1)],
                'box_2': ['smo_{}'.format(cat1)],
                'box_3': ['cgo_{}'.format(cat1)]
                }
        for box, variable in meta.items():
            odata[box] = idata[variable].mean(axis=1)

    elif name == 'noaa':
        odata = pd.DataFrame({}, index=idata.index)
        stations = pd.read_csv(os.path.join(os.path.dirname(dpath),
                                            '../noaa_stations.csv'))
        stations.set_index('code', inplace=True)
        st_sel = ["alt", "ams", "asc", "ask", "azr", "bhd", "bmw", "brw",
                  "cba", "cgo", "cmo", "crz", "eic", "ice", "izo", "key",
                  "kum", "mbc", "mhd", "mlo", "nwr", "psa", "rpb", "shm",
                  "smo", "spo", "stm", "sum", "syo", "thd", "ush", "uta",
                  "uum", "wlg", "zep"]
        st = [s for s in st_sel if s in idata.columns]
        lat = stations['latitude']
        st = {'box_0': [s for s in st if lat[s] >= 30],
              'box_1': [s for s in st if 30 >= lat[s] >= 0],
              'box_2': [s for s in st if 0 >= lat[s] >= -30],
              'box_3': [s for s in st if lat[s] <= -30]}
        for k, v in st.items():
            odata[k] = idata[v].mean(axis=1)

    elif name in ['edgar_txt', 'edgar_nc', 'gfed', 'wetcharts']:
        date_range = sorted(list(idata.keys()))
        if name == 'edgar_txt':
            index = pd.date_range(date_range[0], date_range[-1], freq='1YS')
            res = 0.1
            factor30deg = (
                    np.array([
                        np.sin(np.deg2rad(30+res/2)) - 0.5,
                        0.5 - np.sin(np.deg2rad(30-res/2))
                        ]) /
                    (2*np.sqrt(3)/2*np.sin(np.deg2rad(res/2)))
                    )
        elif name == 'edgar_nc':
            index = pd.date_range(date_range[0], date_range[-1], freq='1YS')
            res = 0.1
        elif name == 'gfed':
            lat = idata['lat']
            date_range.remove('lat')
            index = pd.date_range(date_range[0], date_range[-1], freq='1MS')
            res = 0.25
            area = calc_cell_area([lat+res/2, lat-res/2], res)
            factor30deg = (
                    np.array([
                        np.sin(np.deg2rad(30+res/2)) - 0.5,
                        0.5 - np.sin(np.deg2rad(30-res/2))
                        ]) /
                    (2*np.sqrt(3)/2*np.sin(np.deg2rad(res/2)))
                    )
        elif name == 'wetcharts':
            lat = idata['lat']
            date_range.remove('lat')
            index = pd.date_range(date_range[0], date_range[-1], freq='1MS')
            res = 0.5
            area = calc_cell_area([lat+res/2, lat-res/2], res)
            factor30deg = (
                    np.array([
                        np.sin(np.deg2rad(30+res/2)) - 0.5,
                        0.5 - np.sin(np.deg2rad(30-res/2))
                        ]) /
                    (2*np.sqrt(3)/2*np.sin(np.deg2rad(res/2)))
                    )
        odata = pd.DataFrame(
                0.0, index=index,
                columns=['box_{}'.format(i) for i in range(4)]
                )

        odata.index.name = 'date'
        for date in date_range:
            if name == 'edgar_txt':
                idata[date].reset_index(level=None, inplace=True)
                lat = idata[date]['lat']
                value = idata[date]['emission {} (tons)'.format(date)]
            elif name == 'edgar_nc':
                pass
            elif name == 'gfed':
                value = idata[date] * area
            elif name == 'wetcharts':
                value = idata[date] * area
#            odata.loc[date] = grid2box(value, lat, factor30deg)
            v = grid2box(value, lat, factor30deg)
            for b in odata.columns:
                odata.loc[date, b] = v[b]
    return odata

