# -*- coding: utf-8 -*-
"""
forward.py

AGAGE Box model

The model calculates semi-hemispheric monthly average mole fractions, based on
given emissions, stratospheric and oceanic lifetimes and sink reaction rates.

This code started as a python3 port of the IDL code written by Matt Rigby
available from: https://bitbucket.org/mrghg/agage-12-box-model/

Differences include:
    01. Aside from model_transport_matrix function, the model is flexible to
        have any number of boxes, not just 12(3x4).
    02. All scaling must be preprocessed. The model does not take scaling
        factors
    03. Lifetime is monthly not seasonal.
    04. A conversion factor between year and model-year is 365.25/360.0 not
        365.0/360.0.

Usage:
    01. Import to a python script or ipython session
            import chung_box_model.forward
        and run the functions individually.

Notes:
    01. The input .csv files must include linefeeds (\n) as the line-break.
        Python reader ignores carriage returns (\r) used by old Macs.
        Windows style (\r\n) is fine.

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20171026    EC  Initial code.
20171222    EC  Optimisation and addition of chlorine loss term.
20180106    EC  Inclusion of numba.jit.
20180116    EC  Function name changes; docstring update;
                and use of numpy save files.
20180406    EC  Removed ability to run on its own, to become more Pythonic.
20200822    EC  Packaged.
"""

# Standard Library imports
import numpy as np

# Third party imports
from numba import jit


@jit(nopython=True)
def model_solver_RK4(chi, F, dt):
    r"""Vectorized Runge-Kutta

    Parameters
    ----------
    chi : ndarray
        1d
        Burden (unit: g).
    F : ndarray
        2d, n_box
        Transport matrix.
    dt : float
        Delta t (unit: s).

    Returns
    -------
    chi : ndarray
        1d
        Burden (unit: g).

    """
    A = np.dot(chi, F)
    B = np.dot((chi + dt*A/2.0), F)
    C = np.dot((chi + dt*B/2.0), F)
    D = np.dot((chi + dt*C), F)
    chi += dt/6.0*(A + 2.0*(B + C) + D)
    return chi


@jit(nopython=True)
def model_solver_Lorenz(chi, F, dt):
    r"""Lorenz 4-cycle solver

    Lorenz, E. N. (1971). An N-Cycle Time-Differencing Scheme for Stepwise
    Numerical Integration. Monthly Weather Review, 99(8), 644-648

    Parameters
    ----------
    chi : ndarray
        1d
        Burden (unit: g).
    F : ndarray
        2d, n_box
        Transport matrix.
    dt : float
        Delta t (unit: s).

    Returns
    -------
    chi : ndarray
          1d, n_box
          Burden (unit: g).

    """
    N = 4
    z = np.zeros(12)

    # step 0
    a = 0.0
    b = N/dt  # b = 1/delta_t, delta_t = dt/N

    for x in range(N):
        z = np.multiply(z, a)   # step 1
        z += np.dot(F, chi)     # step 2
        z = z/b                 # step 3
        chi += z                # step 4
        # step 5
        # if a = 0
        #   a = a - 1.0/delta_t = 0 - N/dt
        #   b = b - 1.0/delta_t = N/dt - N/dt
        # if a < 0
        #   a = a + 1.0/dt
        #   b = b + 1.0/dt
        a = (-N + x + 1)/dt
        b = (x + 1)/dt
    return chi


@jit(nopython=True)
def transport_single(i_t, i_v1, t_in, v1_in):
    r"""Calculate transport matrix for single time

    Based on equations in:
        Cunnold, D. M. et al. (1983).
        The Atmospheric Lifetime Experiment 3. Lifetime Methodology and
        Application to Three Years of CFCl3 Data.
        Journal of Geophysical Research, 88(C13), 8379-8400.

    This function outputs a 12x12 matrix (F), calculated by collecting terms in
    the full equation scheme written out on doc_F_equation.txt.
    model transport is then calculated as dc/dt=F##c.

    Parameters
    ----------
    i_t : ndarray
        2d, n_box_stag x 2
        Intersections to apply to the mixing timescales for staggered grids.
    i_v1 : ndarray
        2d, n_box_stag_es x 2
        Intersections to apply to the velocity timescales for staggered grids
        excluding stratosphere.
    t : ndarray
        1d, n_box_stag
        Mixing timescales for staggered grids (unit: s).
    v1 : ndarray
        1d, n_box_stag_es
        Velocity timescales for staggered grids excluding stratosphere
        (unit: s).

    Returns
    -------
    F : ndarray
        2d, n_box x n_box
        Transport matrix

    """
    n_box = int(np.max(i_t) + 1)
    F = np.zeros((n_box, n_box))
    t = np.zeros((n_box, n_box))
    v = np.zeros((n_box, n_box))

    for i in range(0, i_v1.shape[0]):
        v[i_v1[i, 1], i_v1[i, 0]] = 1.0/v1_in[i]
    for i in range(0, i_t.shape[0]):
        t[i_t[i, 1], i_t[i, 0]] = t_in[i]

    F[0, 0] = v[1, 0]/2.0 - v[0, 4]/2.0 - 1.0/t[1, 0] - 1.0/t[0, 4]
    F[1, 0] = v[1, 0]/2.0 + 1.0/t[1, 0]
    F[4, 0] = - v[0, 4]/2.0 + 1.0/t[0, 4]

    F[0, 1] = -v[1, 0]/2.0 + 1.0/t[1, 0]
    F[1, 1] = (v[2, 1]/2.0 - v[1, 5]/2.0 - v[1, 0]/2.0  #
               - 1.0/t[2, 1] - 1.0/t[1, 5] - 1.0/t[1, 0])
    F[2, 1] = v[2, 1]/2.0 + 1.0/t[2, 1]
    F[5, 1] = - v[1, 5]/2.0 + 1.0/t[1, 5]

    F[1, 2] = -v[2, 1]/2.0 + 1.0/t[2, 1]
    F[2, 2] = (v[3, 2]/2.0 - v[2, 6]/2.0 - v[2, 1]/2.0  #
               - 1.0/t[3, 2] - 1.0/t[2, 6] - 1.0/t[2, 1])
    F[3, 2] = v[3, 2]/2.0 + 1.0/t[3, 2]
    F[6, 2] = -1.0*v[2, 6]/2.0 + 1.0/t[2, 6]

    F[2, 3] = -v[3, 2]/2.0 + 1.0/t[3, 2]
    F[3, 3] = -v[3, 7]/2.0 - v[3, 2]/2.0 - 1.0/t[3, 7] - 1.0/t[3, 2]
    F[7, 3] = -v[3, 7]/2.0 + 1.0/t[3, 7]

    F[0, 4] = 5.0/3.0*v[0, 4]/2.0 + 5.0/3.0/t[0, 4]
    F[4, 4] = (5.0/3.0*v[5, 4]/2.0 + 5.0/3.0*v[0, 4]/2.0  #
               - 1.0/t[5, 4] - 5.0/3.0/t[0, 4] - 1.0/t[4, 8])
    F[5, 4] = 5.0/3.0*v[5, 4]/2.0 + 1.0/t[5, 4]
    F[8, 4] = 1.0/t[4, 8]

    F[1, 5] = 5.0/3.0*v[1, 5]/2.0 + 5.0/3.0/t[1, 5]
    F[4, 5] = -5.0/3.0*v[5, 4]/2.0 + 1.0/t[5, 4]
    F[5, 5] = (5.0/3.0*v[6, 5]/2.0 + 5.0/3.0*v[1, 5]/2.0  #
               - 5.0/3.0*v[5, 4]/2.0 - 1.0/t[6, 5]  #
               - 5.0/3.0/t[1, 5] - 1.0/t[5, 9] - 1.0/t[5, 4])
    F[6, 5] = 5.0/3.0*v[6, 5]/2.0 + 1.0/t[6, 5]
    F[9, 5] = 1.0/t[5, 9]

    F[2, 6] = 5.0/3.0*v[2, 6]/2.0 + 5.0/3.0/t[2, 6]
    F[5, 6] = -5.0/3.0*v[6, 5]/2.0 + 1.0/t[6, 5]
    F[6, 6] = (5.0/3.0*v[7, 6]/2.0 + 5.0/3.0*v[2, 6]/2.0  #
               - 5.0/3.0*v[6, 5]/2.0 - 1.0/t[7, 6]  #
               - 5.0/3.0/t[2, 6] - 1.0/t[6, 10] - 1.0/t[6, 5])
    F[7, 6] = 5.0/3.0*v[7, 6]/2.0 + 1.0/t[7, 6]
    F[10, 6] = 1.0/t[6, 10]

    F[3, 7] = 5.0/3.0*v[3, 7]/2.0 + 5.0/3.0/t[3, 7]
    F[6, 7] = -5.0/3.0*v[7, 6]/2.0 + 1.0/t[7, 6]
    F[7, 7] = (5.0/3.0*v[3, 7]/2.0 - 5.0/3.0*v[7, 6]/2.0  #
               - 5.0/3.0/t[3, 7] - 1.0/t[7, 11] - 1.0/t[7, 6])
    F[11, 7] = 1.0/t[7, 11]

    F[4, 8] = 3.0/2.0/t[4, 8]
    F[8, 8] = -1.0/t[9, 8] - 3.0/2.0/t[4, 8]
    F[9, 8] = 1.0/t[9, 8]

    F[5, 9] = 3.0/2.0/t[5, 9]
    F[8, 9] = 1.0/t[9, 8]
    F[9, 9] = -1.0/t[10, 9] - 3.0/2.0/t[5, 9] - 1.0/t[9, 8]
    F[10, 9] = 1.0/t[10, 9]

    F[6, 10] = 3.0/2.0/t[6, 10]
    F[9, 10] = 1.0/t[10, 9]
    F[10, 10] = -1.0/t[11, 10] - 3.0/2.0/t[6, 10] - 1.0/t[10, 9]
    F[11, 10] = 1.0/t[11, 10]

    F[7, 11] = 3.0/2.0/t[7, 11]
    F[10, 11] = 1.0/t[11, 10]
    F[11, 11] = -3.0/2.0/t[7, 11] - 1.0/t[11, 10]

    return F


@jit(nopython=True)
def transport_matrix(i_t, i_v1, t_in, v1_in):
    r"""Calculate transport matrix for all time
    
    Wrapping script to create full transport matrix

    Parameters
    ----------
    i_t : ndarray
        2d, n_box_stag x 2
        Intersections to apply to the mixing timescales for staggered grids.
    i_v1 : ndarray
        2d, n_box_stag_es x 2
        Intersections to apply to the velocity timescales for staggered grids
        excluding stratosphere.
    t : ndarray
        1d, n_box_stag
        Mixing timescales for staggered grids (unit: s).
    v1 : ndarray
        1d, n_box_stag_es
        Velocity timescales for staggered grids excluding stratosphere
        (unit: s).

    Returns
    -------
    F : ndarray
        2d, n_box x n_box
        Transport matrix

    """
    time = v1_in.shape[0]
    n_box = int(np.max(i_t) + 1)
    F = np.zeros((time, n_box, n_box))
    for t in range(time):
        F[t] = transport_single(i_t, i_v1, t_in[t], v1_in[t])
    return F


@jit(nopython=True)
def run_model(c0, q, mol_m, m_atm, loss_factor, ib, dt):
    """Main model code

    Parameters
    ----------
    c0 : ndarray
        2d, n_species x n_box
        Initial conditions of each boxes (unit: nmol mol^-1 ~ppbv).
    q : ndarray
        2d, n_month x n_species x n_box_surface
        Monthly q_mon into the surface boxes (unit: Tg a^-1).
        The length of the simulation is determined by the length of q.
    mol_m : ndarray
        2d, n_species x 1
        Molecular mass (g mol^-1).
    m_atm : ndarray
        1d, n_box
        Mass of air in individual boxes (unit: g).
    loss_factor : ndarray
        3d, n_month x n_loss x n_species x n_box
        Factor to account for sinks. The unit should be (s^-1). Possibilities
        include:
            1. Rate constant for first order reactions
            2. 1 / lifetime
    ib : ndarray
        3d, n_month x n_box x n_box
        Transport parameter matrix.
    dt : float
        Delta time (s).

    Returns
    -------
    c_mon : ndarray
        3d, n_month x n_species x output_boxes
        Mole fractions (unit: nmol mol^-1).
    burden_mon : ndarray
        2d, n_month x n_species x n_box
        The monthly-average global burden_mon (unit: g month^-1).
    q_mon : ndarray
        2d, n_month x n_species x n_box
        The monthly-average mass q_mon (unit: g month^-1).
    loss_mon : ndarray
        3d, n_month x n_loss_terms x n_species x n_box.
        The monthly-average mass loss (unit: g month^-1).
    tau_mon : ndarray
        3d, n_month x (n_loss_terms+total) x n_species x n_box.
        Lifetimes calculated from individual time steps (unit: a).

    """
    # =========================================================================
    #     Set constants
    # =========================================================================
    # Constants
    d2s = 24.0*3600.0  # day to second
    a2s = 365.25*d2s   # year to second
    mol_m_air = 28.97  # dry molecular mass of air in g mol^-1

    n_month = len(q)
    n_box = len(m_atm)
    # n_chem = chem.shape[0]
    # n_species = chem.shape[1]
    n_sink = loss_factor.shape[1]
    n_species = loss_factor.shape[2]

    # =========================================================================
    #     Process input data
    # =========================================================================
    # Sub-time-step
    mi_ti = 30.0*d2s/dt
    if 30.0 % mi_ti == 0.0:
        mi_ti = int(mi_ti)
    else:
        raise Exception("Value Error: dt is not a fraction of 30 days")

    # Initial conditions and time settings
    c = c0*1.0e-9*mol_m/mol_m_air*m_atm  # ppb to g

    start_ti = 0
    end_ti = n_month*mi_ti

    dt_scaled = dt*365.25/360.0

    # Multiply factors
    q_dt = np.zeros((n_month, n_species, n_box))
    q_dt[..., 0:q.shape[-1]] = dt_scaled*q*1.0e12/a2s  # Tg a^-1 to g s^-1
    loss_factor *= dt_scaled
    # Numba does not support einsum nor swapaxes, so doing this outside
    # loss_factor = np.zeros((n_month, n_chem + 1, n_species, n_box))
    #    loss_factor[:, :n_chem] = \
    #            (dt_scaled *
    #             ((arr_chem[0][..., np.newaxis, np.newaxis] * 
    #               np.exp(arr_chem[1][..., np.newaxis, np.newaxis]/T)
    #               ).swapaxes(1, 2).swapaxes(0, 2) *
    #              chem
    #              ).swapaxes(0, 1).swapaxes(1, 2)
    #             )

    #            (dt_scaled *
    #             np.einsum('jk,jkil,ijl ->ijkl',
    #                       arr_chem[0],
    #                       np.exp(arr_chem[1][..., np.newaxis, np.newaxis]/T),
    #                       chem * 1.e6
    #                       )
    #             )

    #            (dt_scaled *
    #             arr_chem[:, 0] *
    #             np.array([np.exp(-arr_chem[i, 1]/T) * chem[i] * 1.e6
    #                       for i in range(n_chem)]) 
    #             )
    #less_factor[:, :n_chem] = dt_scaled/chem_factor
    #loss_factor[:, -1] = dt_scaled/tau/a2s

    # =========================================================================
    #     Output Array
    # =========================================================================
    cnt_mon = np.zeros((n_month, 1, 1))
#    c_mon = np.zeros((n_month, n_box))
#    burden_mon = np.zeros((n_month, n_box))
#    q_mon = np.zeros((n_month, n_box))
#    loss_mon = np.zeros((n_chem + 1, n_month, n_box))
#    tau_mon = np.zeros((n_chem + 2, n_month, n_box))
    c_mon = np.zeros((n_month, n_species, n_box))
    burden_mon = np.zeros((n_month, n_species, n_box))
    q_mon = np.zeros((n_month, n_species, n_box))
    loss_mon = np.zeros((n_month, n_sink, n_species, n_box))
    tau_mon = np.zeros((n_month, n_sink + 1, n_species, n_box))

    # =========================================================================
    #     Run model
    #       Time stepping: Speed matters, not readability
    #       Hence minimise accessing arrays
    # =========================================================================
    for ti in range(start_ti, end_ti):
        mi_ti_cnt = ti % mi_ti
        if not mi_ti_cnt:
            mi = int(ti/mi_ti)

            # Month specific constants
            loss_factor_mi = loss_factor[mi]
            q_mi = q_dt[mi]
            ib_mi = ib[mi]

            # Initialise totals
            cnt_mi = 0.0
            c_mi = np.zeros((n_species, n_box))
            loss_mi = np.zeros((n_sink, n_species, n_box))
            tau_mi = np.zeros((n_sink + 1, n_species, n_box))

        # Transport solver
        c = model_solver_RK4(c/m_atm, ib_mi, dt) * m_atm
        # c = model_solver_Lorenz(c/m_atm, ib_mi, dt) * m_atm

        # Loss
        loss_ti = c*loss_factor_mi

        # Burden
        c += q_mi - loss_ti.sum(axis=0)

        # Monthly totals
        c_mi += c
        loss_mi += loss_ti
#        loss_ti[loss_ti == 0.0] = 1.0e-24
        tau_mi[:-1] += c/loss_ti
        tau_mi[-1] += c/loss_ti.sum(axis=0)
        cnt_mi += 1

        if mi_ti_cnt == mi_ti - 1:
            # Write monthly totals for averages
            c_mon[mi] = c_mi
            loss_mon[mi] = loss_mi
            tau_mon[mi] = tau_mi
            cnt_mon[mi] = cnt_mi

    # Calculate monthly averages
    burden_mon = c_mon.copy()/cnt_mon

    c_mon = burden_mon/mol_m*mol_m_air*1.0e9/m_atm  # g to ppb

    q_mon = q_dt/dt*30.0*d2s

    tau_mon = tau_mon*dt_scaled/a2s/cnt_mon.reshape(n_month, 1, 1, 1)

    return c_mon, burden_mon, q_mon, loss_mon, tau_mon

