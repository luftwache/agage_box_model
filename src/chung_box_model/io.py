# -*- coding: utf-8 -*-
r"""
io.py

Read/write/conversion processes.

Initial author: Edward Chung (s1765003@sms.ed.ac.uk)

Change Log
20171026    EC  Initial code.
20180112    EC  Function updates; docstrings update;
                and numpy save file support.
20200822    EC  Packaged.
"""
# Standard Library imports
import configparser
import numpy as np
import os
import pandas as pd
import scipy.io


def r_csv(fpath, fmt="np"):
    r"""Read csv file

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.
    fmt : {"np", "pd"}, optional
        Format of the output data.

    Returns
    -------
    f : ndarray or DataFrame
        Output data.

    """
    f = pd.read_csv(fpath, sep=',', header=0, skipinitialspace=True)
    if fmt == "np":
        f = f.values
        f = np.ascontiguousarray(f)
    return f


def r_npy(fpath, mmap_mode='r'):
    r"""Read npy file

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.

    Returns
    -------
    f : ndarray
        Output data.

    """
    f = np.load(fpath, mmap_mode=mmap_mode, allow_pickle=False)
    f = np.ascontiguousarray(f)
    return f


def r_npz(fpath):
    r"""Read npz file

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.

    Returns
    -------
    f : ndarray
        Output data.

    """
    d = np.load(fpath, mmap_mode='r', allow_pickle=False)
    for key in d.keys():
        f = np.ascontiguousarray(d[key])
        yield f


def r_idlsave(fpath):
    r"""Read IDL save file

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.

    Returns
    -------
    f : dict
        Dictionary object containing ndarrays.

    """
    f = scipy.io.readsav(fpath, python_dict=True)
    return f


def w_csv(fpath, var, output_boxes='all', fmt='%16.8f'):
    r"""Write ndarray to csv file

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.
    var : ndarray
        Input data for the file.
    output_boxes : {'all', 'surface'} or list
        List of boxes to include in the output.
    fmt : str
        Format of the floats.

    """
    n_box = len(var[0])
    if output_boxes == 'all':
        output_boxes = np.arange(0, n_box)
    elif output_boxes == 'surface':
        output_boxes = np.arange(0, 4)

    var = var[..., output_boxes]

    header_str = []
    for i in output_boxes:
        header_str.append('box_{}'.format(i))
    header_str = ','.join(header_str)

    np.savetxt(fpath, var, fmt=fmt, delimiter=',', newline='\n',
               header=header_str, comments='')


def w_npy(fpath, var):
    r"""Write ndarray to numpy save file

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.
    var : ndarray
        Input data for the file.

    """
    np.save(fpath, var, allow_pickle=False)


def w_npyz(idata, odir):
    r"""Write ndarray to numpy save file

    Parameters
    ----------
    idata : dict
        {prefix: dict or ndarray}
        Input data. Prefix is for the file name.
        Dictionary will result in a npz file, while ndarray will give a npy
        file.
    odir : path-like
        Output directory.

    """
    for k, v in idata.items():
        if v is None:
            continue
        if isinstance(v, np.ndarray):
            np.save(os.path.join(odir, f'{k}.npy'), v, False, False)
        else:
            np.savez(os.path.join(odir, f'{k}.npz'), **v)


def c_csv2npy(fpath1, fpath2):
    r"""Convert csv file to np save file

    Parameters
    ----------
    fpath1 : file-like object, string, or pathlib.Path
        Input csv file.
    fpath2 : file-like object, string, or pathlib.Path
        Output npy file.

    """
    w_npy(fpath2, r_csv(fpath1))


def r_config(config_file, env='default paths'):
    r"""Read config file

    Parameters
    ----------
    config_file : file-like object, string, or pathlib.Path
        Path of the configuration file.
    env : str
        Configuration environment.

    Returns
    -------
    c : dict-like
        Configured objects.

    """
    c = configparser.ConfigParser()
    c.sections()
    c.read(config_file)
    c = c[env]
    return c

