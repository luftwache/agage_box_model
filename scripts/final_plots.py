r"""

Plot all scenarios

"""
# Standard Library imports
import fractions
import numpy as np
import os
import pandas as pd
import re

# Third party imports
from collections import OrderedDict

# Semi-local imports
import chung_box_model.plotter as a_plotter
import chung_box_model.mcmc_CH4 as a_mcmc_CH4
import chung_box_model.mcmc_utils as a_mcmc_utils
import chung_box_model.routines_CH4 as a_routines_CH4


def rgb2gray(c):
    co = np.dot(
            tuple(
                int(i, 16) for i in re.findall('..', c.lstrip('#'))
                ),
            [0.2989, 0.5870, 0.1140]
            )
    return co

def hex2lab(c):
    RGB = np.array([
        int(i, 16) for i in re.findall('..', c.lstrip('#'))
        ])/255
    srgb = np.array([
        64, 30, 15, 3127,  # x
        33, 60, 6, 3290,  # y
        3, 10, 79, 3583,  # z
        2126, 7152, 722, 10000  # Y
        ]).reshape(4, 4)
    Y = np.dot(
        np.linalg.inv(srgb[:3, :3]/srgb[[1], :3]),
        srgb[:3, 3]/srgb[[1], 3]
        )
    Minv = srgb[:3, :3]/srgb[[1], :3] * Y
    XYZ = np.dot(Minv, RGB) * 100.
    d = 6/29
    XYZ_r = np.dot(Minv, np.ones(3)) * 100
    f = np.array([
            t**(1/3) if t > d**3 else
            t/3/d**2 + 4/29
            for t in XYZ/XYZ_r
            ])
    lab = np.array([
        116*f[1] - 16,
        500*(f[0] - f[1]),
        200*(f[1] - f[2])
        ])
    return lab


# =============================================================================
#   Settings
# =============================================================================

mode = 1
odir = f'runs/figs{mode}'

if mode == 0:
    # Scenarios and output directories (input for the plots)
    sces = {
        'Inversion_base': 'runs/sce000000',
        'Inversion_OHinc': 'runs/sce001100',
        'Inversion_OHdec': 'runs/sce001200',
        'Inversion_fixQ': 'runs/sce000100',
        'Inversion_fixOH': 'runs/sce000200',
        'Inversion_alt_tau': 'runs/sce000300',
        'Forward_OH_inc': 'runs/sce001100f',
        'Forward_OH_dec': 'runs/sce001200f',
        'Forward_Q_inc': 'runs/sce001300f',
        'Forward_Q_dec': 'runs/sce001400f',
        'Forward_Qff_inc': 'runs/sce001500f',
        'Forward_Qff_dec': 'runs/sce001600f',
        }
    # Scenario grouping
    sce_groups_id = {
        'Inversion_OH': {1: 'Inversion_OHinc', 2: 'Inversion_OHdec'},
        'Inversion_fix': {1: 'Inversion_fixQ', 2: 'Inversion_fixOH'},
        'Inversion_alt_tau': {1: 'Inversion_base', 2: 'Inversion_alt_tau'},
        'Forward_OH': {1: 'Forward_OH_inc', 2: 'Forward_OH_dec'},
        'Forward_Q': {1: 'Forward_Q_inc', 2: 'Forward_Q_dec'},
        'Forward_Qff': {1: 'Forward_Qff_inc', 2: 'Forward_Qff_dec'},
        }
    sce_description = {
        'Inversion_base': 'Base',
        'Inversion_OHinc': 'OH increase',
        'Inversion_OHdec': 'OH decrease',
        'Inversion_fixQ': 'Q fixed to prior',
        'Inversion_fixOH': 'OH fixed to prior',
        'Inversion_alt_tau': 'Non-OH KIE',
        'Forward_OH_inc': 'OH increase',
        'Forward_OH_dec': 'OH decrease',
        'Forward_Q_inc': 'Q increase',
        'Forward_Q_dec': 'Q decrease',
        'Forward_Qff_inc': r'Q$_{FF}$ increase',
        'Forward_Qff_dec': r'Q$_{FF}$ decrease',
        }
else:
    # Scenarios and output directories (input for the plots)
    sces = {
        'Inversion_base': 'runs/posterior_forward/sce000000f',
        'Inversion_fixQ': 'runs/posterior_forward/sce000100f',
        'Inversion_fixOH': 'runs/posterior_forward/sce000200f',
        #'Inversionf_alt_tau': 'runs/sce000300',
        'Forward_OH_inc': 'runs/exp_forward/sce001100f',
        'Forward_OH_dec': 'runs/exp_forward/sce001200f',
        'Forward_Q_inc': 'runs/exp_forward/sce001300f',
        'Forward_Q_dec': 'runs/exp_forward/sce001400f',
        'Forward_Qff_inc': 'runs/exp_forward/sce001500f',
        'Forward_Qff_dec': 'runs/exp_forward/sce001600f',
        'Forward_OH_inca': 'runs/sce001100f',
        'Forward_OH_deca': 'runs/sce001200f',
        'Forward_Q_inca': 'runs/sce001300f',
        'Forward_Q_deca': 'runs/sce001400f',
        'Forward_Qff_inca': 'runs/sce001500f',
        'Forward_Qff_deca': 'runs/sce001600f',
        }
    # Scenario grouping
    sce_groups_id = {
        'Inversion_fix': {1: 'Inversion_fixQ', 2: 'Inversion_fixOH'},
        #'Inversion_alt_tau': {1: 'Inversion_base', 2: 'Inversion_alt_tau'},
        'Forward_OH': {1: 'Forward_OH_inc', 2: 'Forward_OH_dec'},
        'Forward_Q': {1: 'Forward_Q_inc', 2: 'Forward_Q_dec'},
        'Forward_Qff': {1: 'Forward_Qff_inc', 2: 'Forward_Qff_dec'},
        'Forward_Qa': {1: 'Forward_Q_inca', 2: 'Forward_Q_deca'},
        'Forward_Qffa': {1: 'Forward_Qff_inca', 2: 'Forward_Qff_deca'},
        }
    sce_description = {
        'Inversion_base': 'Base',
        'Inversion_fixQ': 'Q fixed to prior',
        'Inversion_fixOH': 'OH fixed to prior',
        #'Inversion_alt_tau': 'Non-OH KIE',
        'Forward_OH_inc': 'OH increase',
        'Forward_OH_dec': 'OH decrease',
        'Forward_Q_inc': 'Q increase',
        'Forward_Q_dec': 'Q decrease',
        'Forward_Qff_inc': r'Q$_{FF}$ increase',
        'Forward_Qff_dec': r'Q$_{FF}$ decrease',
        'Forward_OH_inca': 'OH increase',
        'Forward_OH_deca': 'OH decrease',
        'Forward_Q_inca': 'Q increase',
        'Forward_Q_deca': 'Q decrease',
        'Forward_Qff_inca': r'Q$_{FF}$ increase',
        'Forward_Qff_deca': r'Q$_{FF}$ decrease',
        }
sce_groups = {
    k: {i: sces[sce] for i, sce in v.items()}
    for k, v in sce_groups_id.items()
    }

# BASE settings file
setfile = 'settings/Inversion_base_D0.json'
# Scenario groups to plot
plot_list = [
    # BASE
    'Inversion_base',
    # Scenario groups
    *sce_groups,
    # BASE theta showing perturbations for forward model experiments.
    'Forward_theta',
    # Combined Forward_Q/Qff
    'Forward_Qs',
    *(['Forward_Qsa'] if mode == 1 else [])
    ]
# =============================================================================
#   Main
# =============================================================================
iset = a_routines_CH4.Settings()
iset.read(setfile)
(
    model, dates, p1, p2, p3, ptres, p1_def, p2_def, p3_def, D_q,
    ) = a_routines_CH4.prepare_scenario(iset)

isos = {
        'C12H4': u'$^{12}$CH$_4$',
        'C13H4': u'$^{13}$CH$_4$',
        'C12H3D': u'$^{12}$CH$_3$D',
        'C13H3D': u'$^{13}$CH$_3$D',
        'C12H2D2': u'$^{12}$CH$_2$D$_2$'
        }
meta = {
        'c': {'ylim': [1.5e3, 2.0e3],
              'lbl': u'Mole Fraction (nmol mol$^{-1}$)',
              'scale': 1.},
        'd_13C': {'ylim': [-48.0, -46.0],
                  'lbl': u'$\delta ^{13}$C (\u2030)',
                  'scale': 1.0},
        'd_2H': {'ylim': [-110.0, -70.0],
                 'lbl': u'$\delta $D (\u2030)',
                 'scale': 1.0},
        'D_C13H3D': {'ylim': [4.0, 5.0],
                     'lbl': u'$\Delta ^{13}$CH$_3$D (\u2030)',
                     'scale': 1.0},
        'D_C12H2D2': {'ylim': [85.0, 95.0],
                      'lbl': u'$\Delta ^{12}$CH$_2$D$_2$ (\u2030)',
                      'scale': 1.0},
        'q': {'ylim': [0., 300.],
              'lbl': u'Total Emissions (Tg yr$^{-1}$)',
              'scale': 1.},
        'q_sum': {'ylim': [300., 800.],
                  'lbl': u'Global Emissions (Tg yr$^{-1}$)',
                  'scale': 1.},
        'dq': {'ylim': [None, None],
               'lbl': '$\delta$SUB, total (\u2030)',
               'scale': 1.},
        'tau': {'ylim': [30, 100],
                'lbl': r'Lifetime SUB (years)',
                'scale': 1.0},
        'OH_a': {'ylim': [-0.4, 0.4],
                 'lbl': r'[OH] anomaly',
                 'scale': 1.0},
        }
meta_diff = {
        'c': {'ylim': [-100., 100],
              'lbl': 'Mole Fraction (nmol mol$^{-1}$)',
              'scale': 1.},
        'd_13C': {'ylim': [-1.0, 1.0],
                  'lbl': u'$\delta ^{13}$C (\u2030)',
                  'scale': 1.0},
        'd_2H': {'ylim': [-10.0, 10.0],
                 'lbl': u'$\delta $D (\u2030)',
                 'scale': 1.0},
        'D_C13H3D': {'ylim': [-1.0, 1.0],
                     'lbl': u'$\Delta ^{13}$CH$_3$D (\u2030)',
                     'scale': 1.0},
        'D_C12H2D2': {'ylim': [-9.6, 9.6],
                      'lbl': u'$\Delta ^{12}$CH$_2$D$_2$ (\u2030)',
                      'scale': 1.0},
        }
plocs1 = OrderedDict([
    ('OH_a', (0, 0)),
    ('q', (1, 0)),
    ('dq_13CH4', (1, 1)),
    ('dq_12CH3D', (1, 2)),
    ])
plocs2 = OrderedDict([
    ('c', (0, 0)),
    ('d_13C', (0, 1)),
    ('d_2H', (0, 2)),
    ])
plocs3 = OrderedDict([
    ('D_C13H3D', (0, 0)),
    ('D_C12H2D2', (0, 1)),
    ])
plocs4 = OrderedDict([
    ('OH_a', (0, 0)),
    ('q_sum', (0, 1)),
    #('dummy', (0, 2)),
    ])
plocs5 = OrderedDict([
    ('c', (0, 0)),
    ('d_13C', (0, 1)),
    ('d_2H', (0, 2)),
    ('D_C13H3D', (1, 0)),
    ('D_C12H2D2', (1, 1)),
    ])
plocs6 = OrderedDict([
    ('c_1', (0, 0)),
    ('d_13C_1', (1, 0)),
    ('d_2H_1', (1, 2)),
    ('D_C13H3D_1', (2, 0)),
    ('D_C12H2D2_1', (2, 2)),
    ('c_2', (0, 1)),
    ('d_13C_2', (1, 1)),
    ('d_2H_2', (1, 3)),
    ('D_C13H3D_2', (2, 1)),
    ('D_C12H2D2_2', (2, 3)),
    ])
plocs7 = OrderedDict([
    ('d_13C_1', (0, 0)),
    ('d_2H_1', (0, 2)),
    ('D_C13H3D_1', (1, 0)),
    ('D_C12H2D2_1', (1, 2)),
    ('d_13C_2', (0, 1)),
    ('d_2H_2', (0, 3)),
    ('D_C13H3D_2', (1, 1)),
    ('D_C12H2D2_2', (1, 3)),
    ])
plocs8 = OrderedDict([
    ('d_13C_1', (0, 0)),
    ('d_2H_1', (0, 1)),
    ('D_C13H3D_1', (1, 0)),
    ('D_C12H2D2_1', (1, 1)),
    ])
pno1 = OrderedDict([
    ('OH_a', 'a)'),
    ('q', 'b)'),
    ('dq_13CH4', 'c)'),
    ('dq_12CH3D', 'd)'),
    ])
pno2 = OrderedDict([
    ('c', 'a)'),
    ('d_13C', 'b)'),
    ('d_2H', 'c)'),
    ])
pno3 = OrderedDict([
    ('D_C13H3D', 'a)'),
    ('D_C12H2D2', 'b)'),
    ])
pno4 = OrderedDict([
    ('OH_a', 'a)'),
    ('q_sum', 'b)'),
    ])
pno5 = OrderedDict([
    ('c', 'a)'),
    ('d_13C', 'b)'),
    ('d_2H', 'c)'),
    ('D_C13H3D', 'd)'),
    ('D_C12H2D2', 'e)'),
    ])
pno6 = OrderedDict([
    ('c_1', 'a1)'),
    ('d_13C_1', 'b1)'),
    ('d_2H_1', 'c1)'),
    ('D_C13H3D_1', 'd1)'),
    ('D_C12H2D2_1', 'e1)'),
    ('c_2', 'a2)'),
    ('d_13C_2', 'b2)'),
    ('d_2H_2', 'c2)'),
    ('D_C13H3D_2', 'd2)'),
    ('D_C12H2D2_2', 'e2)'),
    ])
pno7 = OrderedDict([
    ('d_13C_1', 'a1)'),
    ('d_2H_1', 'b1)'),
    ('D_C13H3D_1', 'c1)'),
    ('D_C12H2D2_1', 'd1)'),
    ('d_13C_2', 'a2)'),
    ('d_2H_2', 'b2)'),
    ('D_C13H3D_2', 'c2)'),
    ('D_C12H2D2_2', 'd2)'),
    ])
pno8 = OrderedDict([
    ('d_13C_1', 'a)'),
    ('d_2H_1', 'b)'),
    ('D_C13H3D_1', 'c)'),
    ('D_C12H2D2_1', 'd)'),
    ])
plot_vars = set(
        list(plocs1)
        + list(plocs2)
        + list(plocs3)
        + list(plocs4)
        + list(plocs5)
        )
plot_tres = {
        k: v for k, v in ptres.items() if k in plot_vars
        }
sectors_q = [k for k in iset.theta_var+iset.model_var if k.startswith('q')]
sectors_d = [k for k in iset.theta_var+iset.model_var if k.startswith('dq')]
plot_tres['q'] = ptres[sectors_q[0]]
plot_tres['q_sum'] = ptres[sectors_q[0]]
for k in ['dq_13CH4', 'dq_12CH3D']:
    plot_tres[k] = ptres[sectors_d[0]]
plot_time = {
        '1M': dates['1M'][0].year + np.arange(0, dates['1M'].size) / 12,
        '1Y': dates['1Y'][0].year + np.arange(0, dates['1Y'].size)
        }
plot_params = {
        k: v
        for k, v in meta.items()
        if k in plot_vars
        }
for n, k in enumerate(['dq_13CH4', 'dq_12CH3D']):
    plot_params[k] = {}
    plot_params[k]['lbl'] = (
            re.sub("SUB", isos[model.iso[n+1]], meta['dq']['lbl'])
            )
plot_params['dq_13CH4']['ylim'] = [-65., -45.]
plot_params['dq_12CH3D']['ylim'] = [-330., -260.]
plot_params_diff = {
        k: v for k, v in meta_diff.items()
        }

phlines = {
    'd_13C': np.arange(*plot_params['d_13C']['ylim'], 0.04),
    'd_2H': np.arange(*plot_params['d_2H']['ylim'], 2.0),
    'D_C13H3D': np.arange(*plot_params['D_C13H3D']['ylim'], 0.4),
    'D_C12H2D2': np.arange(*plot_params['D_C12H2D2']['ylim'], 1.2),
    }
phlines_diff = {
    'd_13C': np.arange(*plot_params_diff['d_13C']['ylim'], 0.04),
    'd_2H': np.arange(*plot_params_diff['d_2H']['ylim'], 2.0),
    'D_C13H3D': np.arange(*plot_params_diff['D_C13H3D']['ylim'], 0.4),
    'D_C12H2D2': np.arange(*plot_params_diff['D_C12H2D2']['ylim'], 1.2),
    }

# =============================================================================
# plot args
# =============================================================================
pargsc1 = [
        '#FC8E50',  # orange
        '#008DE1',  # blue
        '#464E00',  # green
        ]
pargsc2 = [
        '#B02F00', '#F47D11', '#FFCA80',  # orange
        '#0061D2', '#009CFC', '#00DBFF',  # blue
        '#00930A', '#00D56B', '#9FFFC2',  # green
        '#000000', '#909090', '#c8c8c8'  # black
        ]

# parg1 - full BASE
lw1 = 1.0
lw2 = 2.0
pargs1 = {
    'obs': {'c': pargsc1[2], 'ls': '-', 'lw': lw2},
    'model': {'c': pargsc1[1], 'ls': '-', 'lw': lw2},
    'model0': {'c': pargsc1[0], 'ls': '-', 'lw': lw1},
    'r_m': {'c': '#000000', 'ls': '-', 'lw': lw1},
    'r_l': {'c': '#000000', 'ls': '--', 'lw': lw1},
    'r_u': {'c': '#000000', 'ls': '--', 'lw': lw1},
    }

# parg2 - full compare
pargs2 = {
    1: {'c': pargsc1[1], 'ls': '-', 'lw': lw2},
    0: {'c': pargsc1[2], 'ls': '-', 'lw': lw1},
    2: {'c': pargsc1[0], 'ls': '-', 'lw': lw2},
    'r_m': {'c': '#000000', 'ls': '-', 'lw': lw1},
    'r_l': {'c': '#000000', 'ls': '--', 'lw': lw1},
    'r_u': {'c': '#000000', 'ls': '--', 'lw': lw1},
    }

# parg3 - summary BASE
pargs3 = {
    'obs_g': {'c': pargsc2[11], 'ls': '-', 'lw': lw1, 'zorder': 10},
    'model_g': {'c': pargsc2[9], 'ls': '-', 'lw': lw1},
    'model0_g': {'c': pargsc2[9], 'ls': ':', 'lw': lw1},
    'obs_n': {'c': pargsc2[2], 'ls': '-', 'lw': lw1, 'zorder': 10},
    'model_n': {'c': pargsc2[0], 'ls': '-', 'lw': lw1},
    'model0_n': {'c': pargsc2[0], 'ls': ':', 'lw': lw1},
    'obs_s': {'c': pargsc2[5], 'ls': '-', 'lw': lw1, 'zorder': 10},
    'model_s': {'c': pargsc2[3], 'ls': '-', 'lw': lw1},
    'model0_s': {'c': pargsc2[3], 'ls': ':', 'lw': lw1},
    }

# parg4 - summary compare
pargs4 = {
    '0_g': {'c': pargsc2[10], 'ls': '-', 'lw': lw1},
    '1_g': {'c': pargsc2[9], 'ls': '-', 'lw': lw1},
    '2_g': {'c': pargsc2[11], 'ls': '-', 'lw': lw1},
    '0_n': {'c': pargsc2[1], 'ls': '-', 'lw': lw1},
    '1_n': {'c': pargsc2[0], 'ls': '-', 'lw': lw1},
    '2_n': {'c': pargsc2[2], 'ls': '-', 'lw': lw1},
    '0_s': {'c': pargsc2[4], 'ls': '-', 'lw': lw1},
    '1_s': {'c': pargsc2[3], 'ls': '-', 'lw': lw1},
    '2_s': {'c': pargsc2[5], 'ls': '-', 'lw': lw1},
    }

# parg5 - summary compare
pargs5 = {
    'model_g': {'c': pargsc2[9], 'ls': '-', 'lw': lw1},
    'model0_g': {'c': pargsc2[9], 'ls': '--', 'lw': lw1, 'dashes': (5, 5)},
    '1_g': {'c': pargsc2[9], 'ls': ':', 'lw': lw1, 'zorder': 10},
    '2_g': {'c': pargsc2[9], 'ls': ':', 'lw': lw1, 'zorder': 10},
    }

# Summary labels
texts_sum = [
        {
            'x': 0.1,
            'y': 0.95,
            's': 'N. Extratropics',
            'c': pargs3['obs_n']['c'],
            'alpha': 1.0,
            },
        {
            'x': 0.1,
            'y': 0.92,
            's': 'S. Extratropics',
            'c': pargs3['obs_s']['c'],
            'alpha': 1.0,
            }
        ]

# Height
w1 = 6
w2 = 4
h1 = 6.84
h2 = 5.52
h3 = 2.32
h4 = 4.64
h5 = 6.96

# =============================================================================
# Plot
# =============================================================================
overwrite = False

pdatas = {}
pdatas_sum = {}
pdatas_diff = {}
pdata00 = a_routines_CH4.get_plot_data_file(
    'runs/sce000000/', [0, 1, 2, 3], False
    #sces['Inversion_base'], [0, 1, 2, 3], False
    )
y_obs = {
    k: [
        i[k][np.newaxis]
        for i in a_mcmc_utils.calc_bounds(
            {k: p1[k]}, {k: p2[k]}, {k: p3[k]}
            )
        ]
    for k in ['c', 'd_13C', 'd_2H']
    }
if mode == 0:
    y_pse = {
        k: [
            i[k][np.newaxis]
            for i in a_mcmc_utils.calc_bounds(
                {k: pdata00[k]['model'][1]}, {k: p2[k]}, {k: p3[k]}
                )
            ]
        for k in ['c', 'd_13C', 'd_2H']
        }
    y_pse_clump = {
        k: [
            i[k][np.newaxis]
            for i in a_mcmc_utils.calc_bounds(
                {k: pdata00[k]['model'][1]}, {k: p2[k]}, {k: p3[k]}
                )
            ]
        for k in ['D_C13H3D', 'D_C12H2D2']
        }
else:
    y_pse = {
        k: [v[np.newaxis]]
        for k, v in (
            a_mcmc_CH4.calc_y(
                iset.y_var, isos.keys(),
                np.load(os.path.join(sces['Inversion_base'], 'out_c.npy')),
                iset.iso_sel
                )
            ).items()
        if k in ['c', 'd_13C', 'd_2H']
        }
    y_pse_clump = {
        k: [v[np.newaxis]]
        for k, v in (
            a_mcmc_CH4.calc_y(
                iset.y_var, isos.keys(),
                np.load(os.path.join(sces['Inversion_base'], 'out_c.npy')),
                iset.iso_sel
                )
            ).items()
        if k in ['D_C13H3D', 'D_C12H2D2']
        }
    y_model0_clump = {
        k: [v[np.newaxis]]
        for k, v in (
            a_mcmc_CH4.calc_y(
                iset.y_var, isos.keys(),
                np.load(os.path.join('runs/exp_forward/sce000000f', 'out_c.npy')),
                iset.iso_sel
                )
            ).items()
        if k in ['D_C13H3D', 'D_C12H2D2']
        }
    for k in ['c', 'd_13C', 'd_2H']:
        pdata00[k]['model'] = y_pse[k][0]
    for k in ['D_C13H3D', 'D_C12H2D2']:
        pdata00[k]['model'] = y_pse_clump[k][0]
        pdata00[k]['model0'] = y_model0_clump[k][0]
    y_pse_clump2 = {
        k: [v[np.newaxis]]
        for k, v in (
            a_mcmc_CH4.calc_y(
                iset.y_var, isos.keys(),
                np.load(os.path.join('runs/BASE_forward/sce000000f', 'out_c.npy')),
                iset.iso_sel
                )
            ).items()
        if k in ['D_C13H3D', 'D_C12H2D2']
        }

prereq = {
        'Forward_theta': ['Inversion_base', 'Forward_OH', 'Forward_Q'],
        'Forward_Qs': ['Forward_Q', 'Forward_Qff'],
        'Forward_Qsa': ['Forward_Qa', 'Forward_Qffa'],
    }

for sce_group in plot_list:
    print(sce_group)
    if sce_group in prereq:
        if not all([k in pdatas for k in prereq[sce_group]]):
            print('  Skip:  prerequisite not met')
            continue
    po_prefix = os.path.join(odir, f'2_{sce_group}')
    if not overwrite and sce_group in pdatas:
        pdata = pdatas[sce_group]
        pdata_sum = pdatas_sum[sce_group]
        pdata_diff = pdatas_diff[sce_group]
    elif sce_group == 'Forward_theta':
        pdata_sum = {
                'OH_a': {
                    'model_g': pdatas['Inversion_base']['OH_a']['model'],
                    '1_g': pdatas['Forward_OH']['OH_a'][1],
                    '2_g': pdatas['Forward_OH']['OH_a'][2],
                    'model0_g': pdatas['Inversion_base']['OH_a']['model0'][1:2],
                    },
                'q_sum': {
                    'model_g': pdatas['Inversion_base']['q_sum']['model'],
                    '1_g': pdatas['Forward_Q']['q_sum'][1],
                    '2_g': pdatas['Forward_Q']['q_sum'][2],
                    'model0_g': pdatas['Inversion_base']['q_sum']['model0'][1:2],
                    }
                }
    elif sce_group in ['Forward_Qs', 'Forward_Qsa']:
        """
        """
        # Summary - Normal
        pdata_sum = {
            **{f'{k}_1': v for k, v in pdatas_sum['Forward_Q'].items()},
            **{f'{k}_2': v for k, v in pdatas_sum['Forward_Qff'].items()}
            }
        # Summary - Difference
        pdata_diff_sum = {k: {} for k in plocs6}
        for k in set([re.sub('(.*)_[12]$', '\\1', i) for i in plocs6]):
            shp = pdatas['Forward_Q'][k][1].shape
            m = np.s_[:] if shp[0] == 1 else np.s_[1:2]
            pse_obs = {
                **y_pse,
                **(y_pse_clump if sce_group == 'Forward_Qs' else y_pse_clump2)
                }[k]
            Q1 = 'Forward_Q' if sce_group == 'Forward_Qs' else 'Forward_Qa'
            Q2 = 'Forward_Qff' if sce_group == 'Forward_Qs' else 'Forward_Qffa'
            for k1 in pdatas[Q1][k]:
                if shp[-1] == 1:
                    pdata_diff_sum[f'{k}_1'][f'{k1}_g'] = -(
                        pse_obs[0][m, :, :] - pdatas[Q1][k][k1][m, :, :]
                        )
                    pdata_diff_sum[f'{k}_2'][f'{k1}_g'] = -(
                        pse_obs[0][m, :, :] - pdatas[Q2][k][k1][m, :, :]
                        )
                    pdata_diff_sum[f'{k}_1']['r_m'] = -(
                        np.zeros(pdata_diff_sum[f'{k}_1'][f'{k1}_g'].shape)
                        )
                    pdata_diff_sum[f'{k}_2']['r_m'] = -(
                        np.zeros(pdata_diff_sum[f'{k}_2'][f'{k1}_g'].shape)
                        )
                elif shp[-1] == 4:
                    pdata_diff_sum[f'{k}_1'][f'{k1}_n'] = -(
                        pse_obs[0][m, :, :1] - pdatas[Q1][k][k1][m, :, :1]
                        )
                    pdata_diff_sum[f'{k}_1'][f'{k1}_s'] = -(
                        pse_obs[0][m, :, -1:] - pdatas[Q1][k][k1][m, :, -1:]
                        )
                    pdata_diff_sum[f'{k}_1']['r_m'] = -(
                        np.zeros(pdata_diff_sum[f'{k}_1'][f'{k1}_n'].shape)
                        )
                    pdata_diff_sum[f'{k}_2'][f'{k1}_n'] = -(
                        pse_obs[0][m, :, :1] - pdatas[Q2][k][k1][m, :, :1]
                        )
                    pdata_diff_sum[f'{k}_2'][f'{k1}_s'] = -(
                        pse_obs[0][m, :, -1:] - pdatas[Q2][k][k1][m, :, -1:]
                        )
                    pdata_diff_sum[f'{k}_2']['r_m'] = -(
                        np.zeros(pdata_diff_sum[f'{k}_2'][f'{k1}_n'].shape)
                        )
        pdata_diff_sum2 = {k: {} for k in pdata_diff_sum.keys()}
        for k in set([re.sub('(.*)_[12]$', '\\1', i) for i in plocs6]):
            pdata_diff_sum2[f'{k}_1'].update({
                '1_n': pdata_diff_sum[f'{k}_1']['1_n'],
                '1_s': pdata_diff_sum[f'{k}_1']['1_s'],
                'r_m': pdata_diff_sum[f'{k}_1']['r_m'],
                '2_n': pdata_diff_sum[f'{k}_2']['1_n'],
                '2_s': pdata_diff_sum[f'{k}_2']['1_s'],
                })
            pdata_diff_sum2[f'{k}_2'].update({
                '1_n': pdata_diff_sum[f'{k}_1']['2_n'],
                '1_s': pdata_diff_sum[f'{k}_1']['2_s'],
                'r_m': pdata_diff_sum[f'{k}_2']['r_m'],
                '2_n': pdata_diff_sum[f'{k}_2']['2_n'],
                '2_s': pdata_diff_sum[f'{k}_2']['2_s'],
                })
    else:
        """Defaults
        """
        # Normal
        if sce_group == 'Inversion_base':
            pdata = pdata00
        elif sce_group == 'Inversion_alt_tau':
            pdata = a_routines_CH4.get_plot_data_compare(
                    {
                        1: 'runs/sce000000',
                        **sce_groups[sce_group]
                        },
                    model, D_q,
                    {},#{k: pdata00[k]['model'] for k in iset.y_var},
                    {},#np.load('runs/sce000000/out_theta.npz', 'r'),
                    [0, 1, 2, 3],
                    False
                    )
        else:
            pdata = a_routines_CH4.get_plot_data_compare(
                    {
                        #0: 'runs/sce000000',
                        **sce_groups[sce_group]
                        },
                    model, D_q,
                    {}, {},
                    #y_pse, theta00,
                    [0, 1, 2, 3], False
                    )
        pdatas[sce_group] = pdata
        # Difference
        if sce_group == 'Inversion_base':
            pdata_diff = {
                    k: 
                    {
                        **{
                            k1: y_obs[k][0] - v1 for k1, v1 in v.items()
                            if k1 != 'obs'
                            },
                        **{
                            'r_m': y_obs[k][0] - y_obs[k][0],
                            'r_l': y_obs[k][0] - y_obs[k][1],
                            'r_u': y_obs[k][0] - y_obs[k][2]
                            }
                        }
                    for k, v in pdata.items()
                    if k in plocs2
                    }
        elif sce_group in ['Inversion_alt_tau']:
            pdata_diff = {
                    k: 
                    {
                        **{k1: y_obs[k][0] - v1 for k1, v1 in v.items()},
                        **{
                            'r_m': y_obs[k][0] - y_obs[k][0],
                            'r_l': y_obs[k][0] - y_obs[k][1],
                            'r_u': y_obs[k][0] - y_obs[k][2]
                            }
                        }
                    for k, v in pdata.items()
                    if k in plocs2
                    }
        else:
            pdata_diff = {
                    k: 
                    {
                        **{k1: y_pse[k][0] - v1 for k1, v1 in v.items()},
                        **{
                            'r_m': y_obs[k][0] - y_obs[k][0],
                            'r_l': y_obs[k][0] - y_obs[k][1],
                            'r_u': y_obs[k][0] - y_obs[k][2]
                            }
                        }
                    for k, v in pdata.items()
                    if k in plocs2
                    }
        pdatas_diff[sce_group] = pdata_diff
        # Summary
        pdata_sum = {k: {} for k in {**plocs4, **plocs5}}
        for k, v in pdata.items():
            if k not in pdata_sum:
                continue
            for k1, v1 in v.items():
                m = np.s_[:] if v1.shape[0] == 1 else np.s_[1:2]
                if v1.shape[-1] == 1:
                    pdata_sum[k][f'{k1}_g'] = v1[m, :, :]
                elif v1.shape[-1] == 4:
                    pdata_sum[k][f'{k1}_n'] = v1[m, :, :1]
                    pdata_sum[k][f'{k1}_s'] = v1[m, :, -1:]
        pdatas_sum[sce_group] = pdata_sum

    if sce_group == 'Inversion_base':
        pargs_in1 = pargs1
        pargs_in2 = pargs3
    else:
        pargs_in1 = pargs2
        pargs_in2 = pargs4

    # Redefine limits
    if mode == 0 and sce_group in ['Inversion_alt_tau']:
        plot_params['D_C13H3D']['ylim'] = [4., 6.]
        plot_params['D_C12H2D2']['ylim'] = [85., 115.]
        phlines.update({
            'D_C13H3D': np.arange(*plot_params['D_C13H3D']['ylim'], 0.4),
            'D_C12H2D2': np.arange(*plot_params['D_C12H2D2']['ylim'], 1.2),
            })
    elif mode == 0:
        plot_params['D_C13H3D']['ylim'] = [4., 5.]
        plot_params['D_C12H2D2']['ylim'] = [85., 95.]
        phlines.update({
            'D_C13H3D': np.arange(*plot_params['D_C13H3D']['ylim'], 0.4),
            'D_C12H2D2': np.arange(*plot_params['D_C12H2D2']['ylim'], 1.2),
            })
    elif mode == 1 and sce_group.endswith('a'):
        plot_params['D_C13H3D']['ylim'] = [4.0, 5.0]
        plot_params['D_C12H2D2']['ylim'] = [85., 95.]
        phlines.update({
            'D_C13H3D': np.arange(*plot_params['D_C13H3D']['ylim'], 0.4),
            'D_C12H2D2': np.arange(*plot_params['D_C12H2D2']['ylim'], 1.2),
            })
    elif mode == 1:
        plot_params['D_C13H3D']['ylim'] = [2.8, 3.8]
        plot_params['D_C12H2D2']['ylim'] = [85., 95.]
        phlines.update({
            'D_C13H3D': np.arange(*plot_params['D_C13H3D']['ylim'], 0.4),
            'D_C12H2D2': np.arange(*plot_params['D_C12H2D2']['ylim'], 1.2),
            })
    else:
        plot_params['D_C13H3D']['ylim'] = [4., 5.]
        plot_params['D_C12H2D2']['ylim'] = [85., 95.]
        phlines.update({
            'D_C13H3D': np.arange(*plot_params['D_C13H3D']['ylim'], 0.4),
            'D_C12H2D2': np.arange(*plot_params['D_C12H2D2']['ylim'], 1.2),
            })

    # Plots
    if sce_group == 'Forward_theta':
        a_plotter.grid_plot(  # theta
                idata=pdata_sum,
                locs=plocs4,
                tres=plot_tres,
                time=plot_time,
                pargs=pargs5,
                params=plot_params,
                ofile=f'{po_prefix}_s1.png',
                numbering=pno4,
                yticks={},
                size=(w2, h3),
                lbl_hemi=False,
                hlines=phlines,
                legend=[
                    ['model0_g', 'model_g', '1_g'],
                    [
                        ['Prior', {}],
                        ['Posterior', {}],
                        ['Scenarios', {}],
                        ],
                    {
                        'loc': "upper right",
                        'bbox_to_anchor': (1/4, 2/3, (1-0.05)/4, (1-0.15)/3),
                        #'bbox_to_anchor': (2/3, 0/1, 1/3, 1/1),
                        'numpoints': 1, 'fontsize': 6,
                        'handlelength': 3,
                        }
                    ]
                )
        print('  Summary - theta done')
    elif sce_group in ['Forward_Qs', 'Forward_Qsa']:
        """
        """
        # Normal
        a_plotter.grid_plot(  # All
            idata=pdata_sum,
            locs=plocs6,
            tres={
                **{f'{k}_1': v for k, v in plot_tres.items()},
                **{f'{k}_2': v for k, v in plot_tres.items()},
                },
            time=plot_time,
            pargs=pargs4,
            params={
                **{
                    f'{k}_1': {
                        k1: v1 if k1 != 'lbl' else f'{v1} (all sectors)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params.items()
                    },
                **{
                    f'{k}_2': {
                        k1: v1 if k1 != 'lbl' else f'{v1} (fossil only)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params.items()
                    },
                },
            ofile=f'{po_prefix}_s2.png',
            numbering=pno6,
            yticks={
                'd_13C_1': {'ticks': np.arange(-48., -46.+0.5, 0.5)},
                'd_13C_2': {'ticks': np.arange(-48., -46.+0.5, 0.5)}
                },
            size=(w1, h5),
            lbl_hemi=False,
            hlines={
                **{f'{k}_1': v for k, v in phlines.items()},
                **{f'{k}_2': v for k, v in phlines.items()},
                },
            texts=texts_sum,
            legend=[
                ['1_n', '1_s', '2_n', '2_s'],
                [
                    ['Increasing Q (30-90\u00B0N)', {}],
                    ['Increasing Q (30-90\u00B0S)', {}],
                    ['Decreasing Q (30-90\u00B0N)', {}],
                    ['Decreasing Q (30-90\u00B0S)', {}],
                    ],
                {
                    'loc': "upper right",
                    'bbox_to_anchor': (0.5, 0.5, 0.5, 0.5),
                    'numpoints': 1, 'fontsize': 6, 
                    }
                ]
            )
        a_plotter.grid_plot(  # Isotopic only
            idata=pdata_sum,
            locs=plocs7,
            tres={
                **{f'{k}_1': v for k, v in plot_tres.items()},
                **{f'{k}_2': v for k, v in plot_tres.items()},
                },
            time=plot_time,
            pargs={**pargs2, **pargs4},
            params={
                **{
                    f'{k}_1': {
                        k1: v1 if k1 != 'lbl' else f'{v1} (all sectors)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params.items()
                    },
                **{
                    f'{k}_2': {
                        k1: v1 if k1 != 'lbl' else f'{v1} (fossil only)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params.items()
                    },
                },
            ofile=f'{po_prefix}_d3_sum.png',
            numbering=pno7,
            yticks={
                'd_13C_1': {'ticks': np.arange(-48., -46.+0.5, 0.5)},
                'd_13C_2': {'ticks': np.arange(-48., -46.+0.5, 0.5)}
                },
            size=(w1, h4),
            lbl_hemi=False,
            hlines={
                **{f'{k}_1': v for k, v in phlines.items()},
                **{f'{k}_2': v for k, v in phlines.items()},
                },
            texts=texts_sum
            )
        print('  Summary - y done')
        # Difference plot
        a_plotter.grid_plot(  # All
            idata=pdata_diff_sum,
            locs=plocs6,
            tres={
                **{f'{k}_1': v for k, v in plot_tres.items()},
                **{f'{k}_2': v for k, v in plot_tres.items()},
                },
            time=plot_time,
            pargs={**pargs2, **pargs4},
            params={
                **{
                    f'{k}_1': {
                        k1: v1 if k1 != 'lbl' else f'{v1} (all sectors)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params_diff.items()
                    },
                **{
                    f'{k}_2': {
                        k1: v1 if k1 != 'lbl' else f'{v1} (fossil only)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params_diff.items()
                    },
                },
            ofile=f'{po_prefix}_d1_sum.png',
            numbering=pno6,
            yticks={},
            size=(w1, h5),
            lbl_hemi=False,
            hlines={
                **{f'{k}_1': v for k, v in phlines_diff.items()},
                **{f'{k}_2': v for k, v in phlines_diff.items()},
                },
            texts=texts_sum
            )
        a_plotter.grid_plot(  # Isotopic only
            idata=pdata_diff_sum,
            locs=plocs7,
            tres={
                **{f'{k}_1': v for k, v in plot_tres.items()},
                **{f'{k}_2': v for k, v in plot_tres.items()},
                },
            time=plot_time,
            pargs={**pargs2, **pargs4},
            params={
                **{
                    f'{k}_1': {
                        k1: v1 if k1 != 'lbl' else f'Difference in {v1} (all sectors)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params_diff.items()
                    },
                **{
                    f'{k}_2': {
                        k1: v1 if k1 != 'lbl' else f'Difference in {v1} (fossil only)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params_diff.items()
                    },
                },
            ofile=f'{po_prefix}_d2_sum.png',
            numbering=pno7,
            yticks={},
            size=(w1, h4),
            lbl_hemi=False,
            hlines={
                **{f'{k}_1': v for k, v in phlines_diff.items()},
                **{f'{k}_2': v for k, v in phlines_diff.items()},
                },
            #texts=texts_sum,
            legend=[
                ['1_n', '1_s', '2_n', '2_s'],
                [
                    ['Increasing\nQ (30-90\u00B0N)', {}],
                    ['Increasing\nQ (30-90\u00B0S)', {}],
                    ['Decreasing\nQ (30-90\u00B0N)', {}],
                    ['Decreasing\nQ (30-90\u00B0S)', {}],
                    ],
                {
                    'loc': "lower left",
                    'bbox_to_anchor': (1/12, 1.08/2, 2/12, 0.92/2),
                    'numpoints': 1, 'fontsize': 6,
                    'handlelength': 3,
                    'borderpad': 0.2,
                    'labelspacing': 0.2,
                    }
                ]
            )
        a_plotter.grid_plot(  # Isotopic only - alternate
            idata=pdata_diff_sum2,
            locs=plocs7,
            tres={
                **{f'{k}_1': v for k, v in plot_tres.items()},
                **{f'{k}_2': v for k, v in plot_tres.items()},
                },
            time=plot_time,
            pargs={**pargs2, **pargs4},
            params={
                **{
                    f'{k}_1': {
                        k1: v1 if k1 != 'lbl' else f'Difference in {v1} (More Emissions)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params_diff.items()
                    },
                **{
                    f'{k}_2': {
                        k1: v1 if k1 != 'lbl' else f'Difference in {v1} (Less Emissions)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params_diff.items()
                    },
                },
            ofile=f'{po_prefix}_d3_sum.png',
            numbering=pno7,
            yticks={},
            size=(w1, h4),
            lbl_hemi=False,
            hlines={
                **{f'{k}_1': v for k, v in phlines_diff.items()},
                **{f'{k}_2': v for k, v in phlines_diff.items()},
                },
            #texts=texts_sum,
            legend=[
                ['1_n', '1_s', '2_n', '2_s'],
                [
                    ['All sectors\n(30-90\u00B0N)', {}],
                    ['All sectors\n(30-90\u00B0S)', {}],
                    ['Fossil only\n(30-90\u00B0N)', {}],
                    ['Fossil only\n(30-90\u00B0S)', {}],
                    ],
                {
                    'loc': "lower left",
                    'bbox_to_anchor': (1/12, 1.08/2, 2/12, 0.92/2),
                    'numpoints': 1, 'fontsize': 6,
                    'handlelength': 3,
                    'borderpad': 0.2,
                    'labelspacing': 0.2,
                    }
                ]
            )
        a_plotter.grid_plot(  # Isotopic only - alternate - increase only
            idata=pdata_diff_sum2,
            locs=plocs8,
            tres={
                **{f'{k}_1': v for k, v in plot_tres.items()},
                },
            time=plot_time,
            pargs={**pargs2, **pargs4},
            params={
                **{
                    f'{k}_1': {
                        k1: v1 if k1 != 'lbl' else f'Difference in {v1} (More Emissions)'
                        for k1, v1 in v.items()
                        }
                    for k, v in plot_params_diff.items()
                    },
                },
            ofile=f'{po_prefix}_d4_sum.png',
            numbering=pno8,
            yticks={},
            size=(w1, h4),
            lbl_hemi=False,
            hlines={
                **{f'{k}_1': v for k, v in phlines_diff.items()},
                },
            #texts=texts_sum,
            legend=[
                ['1_n', '1_s', '2_n', '2_s'],
                [
                    ['All sectors\n(30-90\u00B0N)', {}],
                    ['All sectors\n(30-90\u00B0S)', {}],
                    ['Fossil only\n(30-90\u00B0N)', {}],
                    ['Fossil only\n(30-90\u00B0S)', {}],
                    ],
                {
                    'loc': "lower left",
                    'bbox_to_anchor': (1/12, 1.08/2, 2/12, 0.92/2),
                    'numpoints': 1, 'fontsize': 6,
                    'handlelength': 3,
                    'borderpad': 0.2,
                    'labelspacing': 0.2,
                    }
                ]
            )
        print('  Summary - y diff done')
    else:
        a_plotter.grid_plot(  # theta
                idata=pdata,
                locs=plocs1,
                tres=plot_tres,
                time=plot_time,
                pargs=pargs_in1,
                params=plot_params,
                ofile=f'{po_prefix}_f1.png',
                numbering=pno1,
                yticks={},
                size=(w1, h1),
                lbl_hemi=True,
                hlines=phlines
                )
        print('  theta_done')
        a_plotter.grid_plot(  # obs
                idata=pdata,
                locs=plocs2,
                tres=plot_tres,
                time=plot_time,
                pargs=pargs_in1,
                params=plot_params,
                ofile=f'{po_prefix}_f2.png',
                numbering=pno2,
                yticks={
                    'd_13C': {'ticks': np.arange(-48., -46.+0.5, 0.5)},
                    },
                size=(w1, h2),
                lbl_hemi=True,
                hlines=phlines
                )
        print('  chi/delta done')
        a_plotter.grid_plot(  # clumped
                idata=pdata,
                locs=plocs3,
                tres=plot_tres,
                time=plot_time,
                pargs=pargs_in1,
                params=plot_params,
                ofile=f'{po_prefix}_f3.png',
                numbering=pno3,
                yticks={},
                size=(w2, h2),
                lbl_hemi=True,
                hlines=phlines
                )
        print('  Delta done')

        # Difference
        a_plotter.grid_plot(
                idata=pdata_diff,
                locs=plocs2,
                tres=plot_tres,
                time=plot_time,
                pargs=pargs_in1,
                params=plot_params_diff,
                ofile=f'{po_prefix}_d1.png',
                numbering=pno2,
                yticks={},
                size=(w1, h2),
                lbl_hemi=True,
                hlines=phlines_diff
                )
        print('  Diff done')

        # Summary
        a_plotter.grid_plot(  # theta
                idata=pdata_sum,
                locs=plocs4,
                tres=plot_tres,
                time=plot_time,
                pargs=pargs_in2,
                params=plot_params,
                ofile=f'{po_prefix}_s1.png',
                numbering=pno4,
                yticks={},
                size=(w2, h3),
                lbl_hemi=False,
                hlines=phlines,
                legend=[
                    (
                        ['model0_g', 'model_g']
                        if sce_group == 'Inversion_base' else
                        ['1_g', '2_g']
                        ),
                    (
                        [
                            ['Prior', {}],
                            ['Posterior', {}],
                            ]
                        if sce_group == 'Inversion_base' else
                        [
                            [
                                sce_description[sce_groups_id[sce_group][1]],
                                {'color': pargsc2[9]}
                                ],
                            [
                                sce_description[sce_groups_id[sce_group][2]],
                                {'color': pargsc2[11]}
                                ],
                            ]
                        ),
                    {
                        'loc': "upper right",
                        'bbox_to_anchor': (1/4, 2/3, (1-0.05)/4, (1-0.15)/3),
                        #'bbox_to_anchor': (2/3, 0/1, 1/3, 1/1),
                        'numpoints': 1, 'fontsize': 6,
                        'handlelength': 3,
                        }
                    ]
                )
        print('  Summary - theta done')
        a_plotter.grid_plot(  # y
                idata=pdata_sum,
                locs=plocs5,
                tres=plot_tres,
                time=plot_time,
                pargs=pargs_in2,
                params=plot_params,
                ofile=f'{po_prefix}_s2.png',
                numbering=pno5,
                yticks={
                    'd_13C': {'ticks': np.arange(-48., -46.+0.5, 0.5)},
                    },
                size=(w1, h4),
                lbl_hemi=False,
                hlines=phlines,
                #texts=texts_sum,
                legend=[
                    (
                        [
                            'obs_n', 'model0_n', 'model_n',
                            'obs_s', 'model0_s', 'model_s'
                            ]
                        if sce_group == 'Inversion_base' else
                        ['1_n', '1_s', '2_n', '2_s']
                        ),
                    (
                        [
                            [u'Observation (30-90\u00B0N)', {}],
                            [u'Prior (30-90\u00B0N)', {}],
                            [u'Posterior (30-90\u00B0N)', {}],
                            [u'Observation (30-90\u00B0S)', {}],
                            [u'Prior (30-90\u00B0S)', {}],
                            [u'Posterior (30-90\u00B0S)', {}],
                            ]
                        if sce_group == 'Inversion_base' else
                        [
                            [
                                f'{sce_description[sce_groups_id[sce_group][1]]}'
                                ' (30-90\u00B0N)',
                                {}
                                ],
                            [
                                f'{sce_description[sce_groups_id[sce_group][1]]}'
                                ' (30-90\u00B0S)',
                                {}
                                ],
                            [
                                f'{sce_description[sce_groups_id[sce_group][2]]}'
                                ' (30-90\u00B0N)',
                                {}
                                ],
                            [
                                f'{sce_description[sce_groups_id[sce_group][2]]}'
                                ' (30-90\u00B0S)',
                                {}
                                ],
                            ]
                        ),
                    {
                        'loc': "center",
                        'bbox_to_anchor': (2/3, 0/2, 1/3, 1/2),
                        'numpoints': 1, 'fontsize': 6,
                        'handlelength': 3,
                        }
                    ]
                )
        print('  Summary - y done')
    # Close figures
    a_plotter.plt.close('all')

# Save numbers
for sce_group, pdata in pdatas.items():
    pd.concat([
        pd.DataFrame(
            np.swapaxes(i, 0, 1),
            columns=pd.MultiIndex.from_tuples([
                (k0, k1, a, b)
                for a in (['global'] if v1.shape[-1] == 1 else [f'box{n}'])
                for b in (
                    ['mu'] if v1.shape[0] == 1 else ['lower', 'mu', 'upper']
                    )
                ]),
            index=pd.date_range('1980-01', '2014-12', freq='1MS')
            )
        for k0, v0 in pdata.items()
        if k0 in plocs5
        for k1, v1 in v0.items()
        for n, i in enumerate(np.moveaxis(v1, 2, 0))
        ], axis=1).rename(
            columns={
                'model0': 'Prior', 'model': 'Posterior', 'obs': 'Observations',
                **({
                    1: sce_description[sce_groups_id[sce_group][1]],
                    2: sce_description[sce_groups_id[sce_group][2]],
                    } if sce_group in sce_groups_id else {})
                }
                ).to_csv(os.path.join(odir, f'{sce_group}_y.csv'))
    pd.concat([
        pd.DataFrame(
            np.swapaxes(i, 0, 1),
            columns=pd.MultiIndex.from_tuples([
                (k0, k1, a, b)
                for a in (['global'] if v1.shape[-1] == 1 else [f'box{n}'])
                for b in (
                    ['mu'] if v1.shape[0] == 1 else ['lower', 'mu', 'upper']
                    )
                ]),
            index=pd.date_range('1980-01', '2014-01', freq='1YS')
            )
        for k0, v0 in pdata.items()
        if k0 in list(plocs1.keys()) + ['q_sum']
        for k1, v1 in v0.items()
        for n, i in enumerate(np.moveaxis(v1, 2, 0))
        ], axis=1).rename(
            columns={
                'model0': 'Prior', 'model': 'Posterior', 'obs': 'Observations',
                **({
                    1: sce_description[sce_groups_id[sce_group][1]],
                    2: sce_description[sce_groups_id[sce_group][2]],
                    } if sce_group in sce_groups_id else {})
                }
                ).to_csv(os.path.join(odir, f'{sce_group}_theta.csv'))

