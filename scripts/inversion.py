r"""

Main inversion script

"""
# Standard Library imports
import argparse
import emcee
import mpi4py.MPI
import multiprocessing
import numpy as np
import os
import shutil
import sys
import time
import textwrap
import re

# Third party imports
from collections import OrderedDict
from schwimmbad import MPIPool

# Semi-local imports
import chung_box_model.io as a_io
import chung_box_model.mcmc_utils as a_mcmc_utils
import chung_box_model.mcmc_CH4 as a_mcmc_CH4
import chung_box_model.plotter as a_plotter
import chung_box_model.routines_CH4 as a_routines


# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-s", required=False, default=None)
parser.add_argument("-o", required=False, default="output")
parser.add_argument("-t", required=False, type=int, default=0)
parser.add_argument("-v", required=False, default="")
parser.add_argument("-f", required=False, type=int, default=0)
parser.add_argument("-n", required=False, default=None)
parser.add_argument("-D03", required=False, default=None)
parser.add_argument("-D04", required=False, default=None)
parser.add_argument("-Dq3", required=False, default=None)
parser.add_argument("-Dq4", required=False, default=None)
args = parser.parse_args()

if args.s is not None:
    iset_file = args.s
else:
    raise Exception('Settings file is not provided.')
test = args.t
validate = args.v
forward_only = args.f
diff_D03 = float(args.D03) if args.D03 is not None else None
diff_D04 = float(args.D04) if args.D04 is not None else None
diff_Dq3 = float(args.Dq3) if args.Dq3 is not None else None
diff_Dq4 = float(args.Dq4) if args.Dq4 is not None else None

# Settings file
iset = a_routines.Settings()
iset.read(iset_file)
if test == 0:
    iset.odir = os.path.join(
            args.o,
            'sce{:02d}{:02d}{:02d}'.format(*iset.sce_run)
            )
else:
    iset.odir = os.path.join(args.o, f'test{test}')
    if test in [1, 2]:
        iset.chain = [0, 0, 0]
    elif test == 3:
        iset.chain = [2, 2, 2]

if forward_only:
    if not iset.chain == [0, 0, 0]:
        iset.chain = [0, 0, 0]
    iset.odir = os.path.join(
            os.path.dirname(iset.odir),
            '{}f'.format(os.path.basename(iset.odir))
            )

if validate:
    iset.chain = [0, 0, 0]
    iset.obs_def = [validate, iset.y_var]
    iset.sce_pre = [validate, iset.theta_var + iset.model_var]
    iset.odir = os.path.join(validate, 'val')

# =============================================================================
#     Array Settings
# =============================================================================
if args.n is not None:
    iset.odir = os.path.join(iset.odir, args.n)

# =============================================================================
#     numpy Settings
# =============================================================================
np.seterr(all='ignore')
np.random.seed(args.n if args.n is not None else 0)

# =============================================================================
#     MPI Settings
# =============================================================================
comm = mpi4py.MPI.COMM_WORLD
size = comm.size
rank = comm.rank
if iset.method == 0:
    if size > 1:
        pool = MPIPool()
        if not pool.is_master():
            pool.wait()
            sys.exit(0)
    else:
        pool = multiprocessing.Pool(processes=2)

# =============================================================================
#     Pre-processing
# =============================================================================
if rank == 0:
    print('Scenario {}'.format(iset.sce_run))
    print('Pre-processing')
(
    model, dates, p1, p2, p3, ptres, p1_def, p2_def, p3_def, D_q,
    ) = a_routines.prepare_scenario(iset)
if diff_D03 is not None:
    iset.D_obs[0] = diff_D03
if diff_D04 is not None:
    iset.D_obs[1] = diff_D04
if diff_Dq3 is not None:
    D_q['dq_total'][:, 0] = diff_Dq3
if diff_Dq4 is not None:
    D_q['dq_total'][:, 1] = diff_Dq4

# =============================================================================
#     Main processing
# =============================================================================
if rank == 0:
    pm, pl, pu = a_mcmc_utils.calc_bounds(p1, p2, p3)
    pm_def, pl_def, pu_def = a_mcmc_utils.calc_bounds(p1_def, p2_def, p3_def)

    state0, dim_theta = a_mcmc_utils.flatten_arrays(
            OrderedDict((k, pm[k]) for k in iset.theta_var)
            )
    L0, c0 = a_mcmc_CH4.model_lnlike(
            state0, model, dates, ptres,
            p1, p2, p3,
            iset.y_var, iset.theta_var, dim_theta,
            iset.pslice, iset.D_obs, D_q, iset.iso_sel
            )
    L_max = sum([
        a_mcmc_utils.ln_like(np.nan_to_num(pm[k]), p1[k], p2[k], p3[k])
        for k in iset.y_var + iset.theta_var
        ])
    print(
            f'  L0      = {L0}',
            f'  L_max   = {L_max}',
            sep='\n'
            )

if not iset.theta_var:
    num = 0
elif iset.method == 0:
    num = np.prod(iset.chain)
else:
    num = 0

if num == 0:
    out_L, out_c, out_theta = None, None, None
elif iset.method == 0:
    np.random.seed(0)
    state = a_mcmc_utils.perturb_arrays_flat(
            a_mcmc_utils.flatten_arrays(
                OrderedDict((k, p1[k]) for k in iset.theta_var)
                )[0],
            a_mcmc_utils.flatten_arrays(
                OrderedDict((k, p2[k]) for k in iset.theta_var)
                )[0],
            OrderedDict((k, p3[k]) for k in iset.theta_var),
            dim=dim_theta,
            ensemble=iset.chain[0]
            )
    sampler = emcee.EnsembleSampler(
            iset.chain[0], state0.size, a_mcmc_CH4.model_lnlike,
            kwargs={
                'model': model, 'dates': dates, 'ptres': ptres,
                'p1': p1, 'p2': p2, 'p3': p3,
                'y_var': iset.y_var, 'theta_var': iset.theta_var,
                'dim_theta': dim_theta,
                'pslice': iset.pslice, 'D_obs': iset.D_obs,
                'D_q': D_q, 'iso_sel': iset.iso_sel
                },
            pool=pool
            )
    for n, i in enumerate(sampler._moves):
        sampler._moves[n].live_dangerously = True
    print(textwrap.indent(
        ('{:6s} {:8s}' + ' {:10s}'*5).format(
            'run_no', 'time', 'max', 'q84', 'q50', 'q16', 'min'
            ),
        ' '*4
        ))
    for i in range(iset.chain[2]):
        sampler.reset()
        t0 = time.time()
        sampler.run_mcmc(
            state,
            iset.chain[1],
            skip_initial_state_check=True
            )
        t1 = time.time()
        state = sampler.chain[:, -1]
        L_last = sampler.lnprobability[:, -1]
        print(textwrap.indent(
            ('{:6d} {:8.2f}' + ' {:10.3e}'*5).format(
                i, t1-t0, *np.percentile(L_last, [100, 84, 50, 16, 0])
                ),
            ' '*4
            ))
    pool.close()
    if size == 1:
        pool.join()
    out_L = np.swapaxes(sampler.lnprobability, 0, 1)
    out_c = np.array(sampler.blobs)
    out_theta = a_mcmc_utils.expand_arrays(
            np.swapaxes(sampler.chain, 0, 1), dim_theta
            )
else:
    out_L, out_c, out_theta = L0[np.newaxis], c0[np.newaxis], None

# =============================================================================
#     Post-processing
# =============================================================================
if test == 1:
    sys.exit(0)
if rank == 0:
    print('Post-processing')

try:
    if os.path.exists(iset.odir):
        shutil.rmtree(iset.odir)
    os.makedirs(iset.odir)
except Exception as err:
    print(err)
    print("Could not replace folder, overwriting instead")

a_io.w_npyz(
        idata={
            'out_L': out_L,
            'out_c': out_c if out_c is not None else c0,
            'out_theta': out_theta,
            'p1': p1,
            'p2': p2,
            'p3': p3,
            'p1_def': p1_def,
            'p2_def': p2_def,
            'p3_def': p3_def
            },
        odir=iset.odir
        )
iset.write(os.path.join(iset.odir, 'settings.json'))

meta = {
        'c': {'ylim': [1.5e3, 2.0e3],
              'lbl': 'Mixing Ratio (ppb)',
              'scale': 1.},
        'd_13C': {'ylim': [-48.0, -46.0],
                  'lbl': u'$\delta ^{13}$C (\u2030)',
                  'scale': 1.0},
        'd_2H': {'ylim': [-110.0, -70.0],
                 'lbl': u'$\delta $D (\u2030)',
                 'scale': 1.0},
        'D_C13H3D': {'ylim':
                        [np.floor(iset.D_obs[0]), np.ceil(iset.D_obs[0])]
                        if iset.D_obs and iset.D_obs[0] is not None else
                        [4.0, 5.0]
                        if diff_D03 is None else
                        [np.floor(diff_D03), np.ceil(diff_D03)],
                     'lbl': u'$\Delta ^{13}$CH$_3$D (\u2030)',
                     'scale': 1.0},
        'D_C12H2D2': {'ylim': [85.0, 95.0],
                      'lbl': u'$\Delta ^{12}$CH$_2$D$_2$ (\u2030)',
                      'scale': 1.0},
        'q': {'ylim': [None, None],
              'lbl': u'Q, total (Tg/Yr)',
              'scale': 1.},
        'dq': {'ylim': [None, None],
               'lbl': '$\delta$ SUB, total (\u2030)',
               'scale': 1.},
        'tau': {'ylim': [30, 100],
                'lbl': r'Lifetime SUB (years)',
                'scale': 1.0},
        'OH_a': {'ylim': [-0.4, 0.4],
                 'lbl': r'[OH] anomaly',
                 'scale': 1.0},
        }
isos = {
        'C12H4': u'$^{12}$CH$_4$',
        'C13H4': u'$^{13}$CH$_4$',
        'C12H3D': u'$^{12}$CH$_3$D',
        'C13H3D': u'$^{13}$CH$_3$D',
        'C12H2D2': u'$^{12}$CH$_2$D$_2$'
        }
pargs = {
        'model': {'c': '#008DE1', 'lw': 0.5},
        'obs': {'c': '#464E00', 'lw': 0.5},
        'model0': {'c': '#FC8E50', 'lw': 0.5},
        }
colours = {
        'model': '#008DE1',
        'obs': '#464E00',
        'model0': '#FC8E50',
        #'model': '#0077BB',
        #'obs': '#009988',
        #'model0': '#EE7733',
        }

plot_data = a_routines.get_plot_data(
        isets={
            'model0': iset,
            'model': iset,
            'obs': iset
            },
        y_vars={
            'model0': iset.y_var,
            'model': iset.y_var,
            'obs': iset.y_var
            },
        theta_vars={
            'model0': iset.theta_var,
            'model': iset.theta_var
            },
        model_vars={
            'model0': iset.model_var,
            'model': iset.model_var
            },
        cs={
            'model0': c0,
            'model': out_c
            },
        ys={
            'obs': [p1, p2, p3]
            },
        thetas={
            'model0': [p1, p2, p3],
            'model': out_theta
            },
        constant={
            'model0': [
                p1_def, p2_def, p3_def
                ],
            'model': [
                p1_def, p2_def, p3_def
                ]
            },
        D_q={
            'model0': D_q,
            'model': D_q
            },
        R_std = model.R_std,
        M = model.mol_m,
        boxes=[0, 3]
        )

plot_locs = OrderedDict([
    ('c', (0, 0)),
    ('d_13C', (0, 1)),
    ('d_2H', (0, 2)),
    ('D_C13H3D', (1, 0)),
    ('D_C12H2D2', (1, 1)),
    ('OH_a', (2, 0)),
    ('q', (2, 1)),
    ('dq_13CH4', (3, 0)),
    ('dq_12CH3D', (3, 1))
    ])
plot_tres = {k: v for k, v in ptres.items() if k in plot_locs}
sectors_q = [k for k in iset.theta_var+iset.model_var if k.startswith('q')]
sectors_d = [k for k in iset.theta_var+iset.model_var if k.startswith('dq')]
plot_tres['q'] = ptres[sectors_q[0]]
for k in ['dq_13CH4', 'dq_12CH3D']:
    plot_tres[k] = ptres[sectors_d[0]]
plot_time = {
    '1M': dates['1M'][0].year + np.arange(0, dates['1M'].size) / 12,
    '1Y': dates['1Y'][0].year + np.arange(0, dates['1Y'].size)
    }
plot_params = {
        k: v
        for k, v in meta.items()
        if k in plot_locs
        }
for n, k in enumerate(['dq_13CH4', 'dq_12CH3D']):
    plot_params[k] = {}
    plot_params[k]['lbl'] = (
            re.sub("SUB", isos[model.iso[n+1]], meta['dq']['lbl'])
            )
plot_params['dq_13CH4']['ylim'] = [-65, -45]
plot_params['dq_12CH3D']['ylim'] = [-330, -260]
if plot_data['q']['model0'].shape[-1] == 1:
    plot_params['q']['ylim'] = [300., 800.]

a_plotter.grid_plot(
    idata=plot_data,
    locs=plot_locs,
    tres=plot_tres,
    time=plot_time,
    pargs=pargs,
    params=plot_params,
    ofile=os.path.join(iset.odir, 'fig.png')
    )

a_plotter.plt.close('all')

print(' '*2 + 'Output to {}'.format(iset.odir))

