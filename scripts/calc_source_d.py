import numpy as np
import pandas as pd
from chung_box_model.chem_CH4 import CH4_table


def R2D(R):
    D = R * 0.
    D[1] = (R[1]/R[1] - 1.) * 1.e3
    D[2] = (R[2]/R[2] - 1.) * 1.e3
    D[3] = (R[3]/R[1]/R[2] - 1.) * 1.e3
    D[4] = (R[4]/R[2]/R[2] * 8. / 3. - 1.) * 1.e3
    return D


def Dd2R(D, d):
    R = (d * 1.e-3 + 1.) * R_std
    R[3] = (D[3] * 1.e-3 + 1.) * R[1] * R[2]
    R[4] = (D[4] * 1.e-3 + 1.) * R[2] * R[2] * 3. / 8.
    return R


sectors = {
        'Rice_paddies': [110, -63, -320, -528.8, -359.2],
        'Ruminants': [80, -60.5, -330, -549.7, -369.5],
        'Natural_gas': [45, -44, -180, -322.5, -213.7],
        'Coal': [35, -37, -140, -254.8, -169.4],
        'Biomass_burning': [41, -24.6, -225, -394.83, -241.8],
        'Boreal_wetlands': [38, -62, -380, -607.2, -414.87],
        'Tropical_wetlands': [77, -58.9, -360, -582, -394.1],
        'Termites': [16, -63, -390, -620.3, -425],
        'Landfills': [40, -55, -310, -522.5, -346.9],
        'Ocean': [10, -58, -220, -379.1, -260.9],
        'Fresh_water': [4, -53.8, -385, -614.5, -414.8],
        'Gas_hydrates': [4, -62.5, -190, -338.9, -238.4],
        #'Geological': [0, -41.8, -200, -355.2, -231.2],
        'Geological': [50, -41.8, -200, -355.2, -231.2],
        'AMP': [40, -51.2, -260, -450.7, -296.8],
        }

cats = {
        'qtot': sectors.keys(),
        'ftot': [
            'Natural_gas', 'Coal', 'Gas_hydrates', 'Geological'
            ],
        'mant': [
            'Rice_paddies', 'Ruminants', 'Landfills'
            ],
        'mnat': [
            'Boreal_wetlands', 'Tropical_wetlands', 'Termites',
            'Ocean', 'Fresh_water', 'AMP'
            ],
        'btot': [
            'Biomass_burning'
            ]
        }

dq = {
    'dq_total': [0.0, -54.2, -295.0, -330.4, -492.8],
    'dq_ftot': [0.0, -41.9, -177.3, -209.4, -317.5],
    'dq_mant': [0.0, -60.7, -321.7, -360.6, -535.0],
    'dq_mnat': [0.0, -58.1, -338.1, -375.3, -551.8],
    'dq_btot': [0.0, -24.6, -225.0, -241.8, -394.8]
    }

R_std = np.array(CH4_table['R_std'])
M = np.array(CH4_table['mol_m'])

s = pd.DataFrame.from_dict(
    sectors, orient='index', columns=['Q', 1, 2, 4, 3]
    ).reindex(columns=['Q', 1, 2, 3, 4])

# Main
d = np.zeros(s.shape)
d[:, 1:] = np.array(s)[:, 1:]

R = (d * 1.e-3 + 1.) * R_std

A = pd.DataFrame(
    R / R.sum(axis=1, keepdims=True), index=s.index, columns=[0, 1, 2, 3, 4]
    )

# Ratio method
rm_Qi = s[['Q']].values * A / np.sum(A.values * M, axis=1, keepdims=True)
rm_Qi_cat = pd.DataFrame.from_dict({
    cat: rm_Qi.loc[v].sum(0)
    for cat, v in cats.items()
    }, orient='index')
rm_R = rm_Qi_cat / rm_Qi_cat[[0]].values
rm_d = (rm_R / R_std - 1.) * 1.e3
rm_D = R2D(rm_R)

# Haghnegahdar et al 2017 total
ha_d = np.array(dq['dq_total'])
ha_R = (ha_d * 1.e-3 + 1.) * R_std
ha_D = R2D(ha_R)

# Mass balance method
mb_d = pd.concat(
    [
        pd.Series(0., index=cats.keys(), name=0),
        pd.DataFrame.from_dict({
            cat: (
                s.loc[v][[1, 2, 3, 4]]
                * s.loc[v][['Q']].values / s.loc[v]['Q'].sum()
                ).sum(0)
            for cat, v in cats.items()
            }, orient='index')
        ],
    axis=1
    )
mb_R = (mb_d * 1.e-3 + 1.) * R_std
mb_D = R2D(mb_R)

# Douglas 2020 D
d2_D = mb_D.copy()
d2_D.loc['ftot', 3] = 2.8
d2_D.loc['mant', 3] = 1.0
d2_D.loc['mnat', 3] = 2.8
d2_R = Dd2R(d2_D, mb_d)
d2_A = d2_R.div(d2_R.sum(1), axis=0)
d2_Q_cat = pd.DataFrame.from_dict({
    cat: s.loc[v, ['Q']].sum(0)
    for cat, v in cats.items()
    }, orient='index').loc[cats]
d2_Qi_cat = d2_Q_cat.values * d2_A / np.sum(d2_A.values * M, axis=1, keepdims=True)        
d2_Qi_cat.loc['qtot'] = d2_Qi_cat.loc[[c for c in cats if c != 'qtot']].sum(0)
d2_R.loc['qtot'] = d2_Qi_cat.loc['qtot'] / d2_Qi_cat.loc['qtot', 0]
d2_D = R2D(d2_R)

